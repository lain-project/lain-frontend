# Lain backend

## Montaje del entorno de desarrollo

1. Instalar [nodeJS](https://nodejs.org/en/) en su version 16 o [nvm](https://github.com/nvm-sh/nvm).
2. Dentro de la carpeta del projecto ejecutar `npm i`
3. Una vez finalizada la instalación de las dependencias ejecutar `npm run dev`
## Validación

Para validar que el servidor esta arriba se debe acceder a la direccion [http://localhost:3000/docs](http://localhost:3000/docs)