import './style.css'
export const LoginButton = ({ label, type }: any) => {

    return (
        <button type={type} className="w-100 button gradient label-gradient">
            {label}
        </button>
    )
}