import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { InputText } from 'primereact/inputtext'
import { useState } from 'react'

interface TableProps {
  tableRef: React.RefObject<DataTable>
  data: any[]
  title: string
  children: any
  loading?: boolean
  globalFilter?: any
  emptyMessage?: string
  setGlobalFilter?: (value: any) => void
  onSelectRow?: (value: any[]) => void
  selection?: boolean
  footerTitle?: string
}

export const Table = ({ tableRef, data, title, loading, children, emptyMessage = 'No existen datos', onSelectRow, selection = false, footerTitle = '' }: TableProps) => {
  const [selectedRows, setSelectedRows] = useState<any[]>()
  const [globalFilter, setGlobalFilter] = useState<any>()

  const header = (
    <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
      <h5 className="m-0">{title}</h5>
      <span className="block mt-2 md:mt-0 p-input-icon-left">
        <i className="pi pi-search" />
        <InputText
          onInput={(e: any) => setGlobalFilter(e.target.value)}
          type="search"
          placeholder="Buscar..." />
      </span>
    </div>
  )

  // const rolTemplate = (rowData: any) => {
  //     const color = rowData.rol_name === 'ADMIN' ? 'p-badge-success' : 'p-badge-info';
  //     return <span className={`p-badge ${color}`}>{rowData.rol_name}</span>;
  // }

  // const statusTemplate = (rowData: any) => {
  //     const color = rowData.status === 1 ? 'success' : 'danger';
  //     const msg = rowData.status === 1 ? 'Active' : 'Inactive';
  //     return <span className={`custom-badge ${color}`}>{msg}</span>;
  // }

  //     const imageBodyTemplate = (rowData: any) => {
  //         const image  = rowData.avatar? rowData.avatar: '/images/avatar/admin.png';
  //         return (
  //             <>
  //                 <img src={image} alt={rowData.avatar} className="avatar" />
  //             </>
  // { /*    ) */ }
  // { /*    } */ }
  // { /*    const actionBodyTemplate = (rowData: any) => { */ }
  // { /*        return ( */ }
  // { /*            <div className="actions"> */ }
  // { /*            <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2" onClick={() => console.log(rowData)} /> */ }
  // { /*        <Button icon="pi pi-trash" className="p-button-rounded p-button-warning mt-2" onClick={() => console.log('confirm' + rowData)} /> */ }
  // { /*        </div> */ }
  //     );
  //     }

  const onSelectionChange = (value: any) => {
    console.log(value)
    setSelectedRows(value)
    if (onSelectRow) {
      onSelectRow(value)
    }
  }

  return (
    <DataTable
      ref={tableRef}
      className="border-radius"
      value={data}
      selection={selectedRows}
      onSelectionChange={(e) => onSelectionChange(e.value)}
      dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
      paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
      currentPageReportTemplate={`Mostrando {first} al {last} de {totalRecords} ${footerTitle}`}
      globalFilter={globalFilter}
      loading={loading}
      emptyMessage={emptyMessage} header={header} responsiveLayout="scroll">
      {selection &&
        <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
      }
      {children}
    </DataTable>
  )
}
