import { Button } from 'primereact/button'

export const ButtonNew = ({ onClick }: any) => (
    <div className="my-2">
      <Button label="Nuevo" icon="pi pi-plus" className="p-button-success mr-2" onClick={onClick} />
    </div>
)
