import { GlassProps } from "./types"
import { InputText } from "primereact/inputtext"
import React, { useEffect, useState } from "react"
import "./styles.css"

export const GlassInput = ({title, id, type, className, errorMessage,error, onChange, name}: GlassProps) => {

    const [errorValue, setErrorValue] = useState(error);

    useEffect(() => {
        setErrorValue(error);
    }, [error])

    
    const handleError = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        onChange?.apply(null, [event])
        if(value.length === 0 || value === ''){
            setErrorValue(true)
            return
        }
        setErrorValue(false)
    }

    return (
        <span className={className + " p-float-label w-100"}>
            <InputText aria-describedby={id} id={id} 
                autoComplete="off"
                name={name}
                className={errorValue? " p-invalid input  " : "input"} 
                type={type} 
                onChange={handleError}
             />
            <label htmlFor={id}>{title}</label>
            {errorValue && <small id={id} className="p-error block">{errorMessage}</small>}
        </span>
    )
}