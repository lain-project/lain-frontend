import React from "react";

export type GlassProps = {
    id?: string,
    title?: string,
    type?: string,
    className?: string,
    error?: boolean,
    errorMessage?: string,
    value?: string
    name?: string   
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
    onValidateError?: (event: React.ChangeEvent<HTMLInputElement>) => void
}
