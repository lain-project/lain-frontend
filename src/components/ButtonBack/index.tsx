import { useNavigation } from '@/hooks/useNavigation.hook'
import { Button } from 'primereact/button'

// @ts-ignore
export const ButtonBack = ({ onClick }?: any) => {
  const { volver } = useNavigation()

  return (
      <>
        <div className="my-2">
          <Button
            label="Volver"
            icon="pi pi-arrow-left"
            className="p-button-danger mr-2"
            onClick={() => {
              onClick && onClick()
              volver()
            }}
          />
        </div>
      </>
  )
}
