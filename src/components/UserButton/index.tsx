import { Menu } from 'primereact/menu'
import { Toast } from 'primereact/toast'
import { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { useStore } from '../../zustand/store'

// eslint-disable-next-line no-undef
export const UserButton: React.FC<any> = () => {
  const toast = useRef(null)
  const menu = useRef<any>(null)

  const navigate = useNavigate()
  const { setLogin } = useStore()

  const handleClick = () => {
    // eslint-disable-next-line no-undef
    localStorage.removeItem('token')
    // eslint-disable-next-line no-undef
    localStorage.removeItem('tokenneuralaction')
    setLogin(false)
    navigate('/login')
  }

  const items = [
    {
      label: 'Cerrar Session',
      icon: 'pi pi-power-off',
      command: handleClick
    }
  ]
  return (
        <>
            <Toast ref={toast} />
            <Menu model={items} popup ref={menu} id="popup_menu" />
            <button className="p-link layout-topbar-button" onClick={(event) => menu.current.toggle(event)}
                aria-controls="popup_menu" aria-haspopup>
                <i className="pi pi-user" />
                <span>Perfil</span>
            </button>
        </>
  )
}
