export const Footer = (props: any) => {

    return (
        <div className="layout-footer">
            <img src="/assets/layout/images/wired.png" alt="Logo" height="20" className="mr-2" />
            by
            <span className="font-medium ml-2">Himura</span>
        </div>
    );
}
