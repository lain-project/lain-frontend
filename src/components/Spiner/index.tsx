export const Spiner = () => (
  <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}/>
)
