import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { LoginPage } from '../pages/LoginPage'
import { DasboardRoutes } from './DashBoardRoutes'
import { PrivateRoute } from './PrivateRoute'
import { PublicRoute } from './PublicRoute'

export const AppRouter = () => {
  return (
        <BrowserRouter>
            <Routes>
                <Route path="/login" element={
                    <PublicRoute>
                        <LoginPage />
                    </PublicRoute>
                }/>

                <Route path="/*" element={
                    <PrivateRoute>
                        <DasboardRoutes/>
                    </PrivateRoute>
                }/>
            </Routes>
        </BrowserRouter>
  )
}
