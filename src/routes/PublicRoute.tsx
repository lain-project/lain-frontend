import { Navigate, useLocation } from 'react-router-dom'
import { useStore } from '../zustand/store'

export const PublicRoute = ({ children }: any) => {
  const { user } = useStore()

  return user.logged
    ? <Navigate to="/"/>
    : children
}
