import { Navigate, useLocation } from 'react-router-dom'
import { useStore } from '../zustand/store'
import React from 'react'

export const PrivateRoute: React.FC<any> = ({ children }: any) => {
  const { user } = useStore()

  const location = useLocation()

  console.log(location)

  return user.logged
    ? children
    : <Navigate to="/login"/>
}
