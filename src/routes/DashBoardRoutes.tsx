import { Routes, Route } from 'react-router-dom'
import DashboardLayout from '../layout/DashboardLayout'
import { HomePage } from '../pages/HomePage'
import { StudyPlanPage } from '../pages/StudyPlanPage'
import { UsersPage } from '../pages/UsersPage'
import { MateriaPage } from '@/pages/MateriaPage'
import { DocentePage } from '@/pages/DocentePage'
import { PropuestaPage } from '@/pages/PropuestaPage'
import { UnidadesPage } from '@/pages/UnidadPage'
import { ContenidoPage } from '@/pages/ContenidoPage'
import { ContenidoForm } from '@/pages/ContenidoPage/components/ContenidoForm'
import { BibliografiaPage } from '@/pages/BibliografiaPage'
import { CorrelativaPage } from '@/pages/CorrelativaPage'
import { useEffect, useState } from 'react'
import { useStore } from '@/zustand/store'
import { ReportePage } from '@/pages/ReportePage'
import { ChatbotPage } from '@/pages/ChatbotPage'
import {EntidadPage} from "@/pages/ChatbotPage/EntidadPage";

export const DasboardRoutes: React.FC<any> = () => {
  const [isAdmin, setIsAdmin] = useState(false)
  const { user } = useStore()
  useEffect(() => {
    setIsAdmin(user.rolname === 'ADMIN')
    console.log(user.rolname)
  }, [])
  return (
    <>
      <DashboardLayout>

        <Routes>
          <Route path="/" element={<HomePage/>}/>
          {
            isAdmin
              ? <Route path="/users" element={<UsersPage/>}/>
              : <Route path="/" element={<HomePage/>}/>
          }
          <Route path="/study-plans" element={<StudyPlanPage/>}/>
          <Route path="/study-plans/:id" element={<MateriaPage />}/>
          <Route path="/materia/:id/docentes" element={<DocentePage />}/>
          <Route path="/materia/:id/propuesta" element={<PropuestaPage />}/>
          <Route path="/materia/:id/unidades" element={<UnidadesPage />} />
          <Route path="/materia/:id/unidades/contenido" element={<ContenidoPage />} />
          <Route path="/materia/:id/unidades/contenido/agregar" element={<ContenidoForm />} />
          <Route path="/materia/:id/bibliografia" element={<BibliografiaPage />} />
          <Route path="/materia/:id/correlativa" element={<CorrelativaPage />} />
          <Route path="/chatbot" element={<ChatbotPage />} />
          <Route path="/chatbot/:id" element={<EntidadPage/>} />
          <Route path="/reportes" element={<ReportePage/>} />
        </Routes>
      </DashboardLayout>

    </>
  )
}
