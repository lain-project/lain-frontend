import axios from 'axios'
import { headers, neuralactionsToken } from '@/api/headers.service'
import { Bibliografia } from '@/model/bibliografia.model'

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/bibliografia`

export const obtenerBibliografia = async (idMateria: string): Promise<Bibliografia[]> => {
  try {
    const response = await axios.get<Bibliografia[]>(`${URL}/${idMateria}`, {
      headers: headers()
    })
    return response.data as Bibliografia[]
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const asignarBibliografia = async (bibliografia: Bibliografia): Promise<boolean> => {
  try {
    const body = {
      ...bibliografia,
      ...neuralactionsToken()
    }
    const response = await axios.post<boolean>(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const actualizarBibliografia = async (bibliografia: Bibliografia): Promise<boolean> => {
  try {
    const body = {
      ...bibliografia,
      ...neuralactionsToken()
    }
    const response = await axios.put<boolean>(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.message)
  }
}

export const eliminarBibliograia = async (idBibliografia?: string): Promise<Bibliografia> => {
  try {
    const response = await axios.delete<Bibliografia>(`${URL}/${idBibliografia}`, {
      headers: headers()
    })
    return response.data as Bibliografia
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
