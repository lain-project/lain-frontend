import { Token, TokenMapper } from '../model/token'
import { User } from '../model/user'

const url = `${import.meta.env.VITE_APP_BASE_URL}/auth`

export const login = async (user: User): Promise<Token> => {
  const response = await fetch(`${url}/login`, {
    method: 'POST',
    body: JSON.stringify(user),
    headers: { 'Content-Type': 'application/json' }
  })

  if (response.status === 500) {
    throw new Error('Internal Server Error')
  }

  if (response.status === 404) {
    throw new Error('Username y/o password incorrectos')
  }
  if (response.status === 401) {
    throw new Error('Username y/o password incorrectos')
  }
  const tokenRest = await response.json()

  return TokenMapper.toToken(tokenRest)
}

export const googleLogin = async (tokenId: string): Promise<Token> => {
  const response = await fetch(`${url}/google?token_id=${tokenId}`, {
    method: 'POST',
    body: JSON.stringify({ tokenId }),
    headers: { 'Content-Type': 'application/json' }
  })
  if (response.status === 500 || response.status === 404) {
    const { message } = await response.json()
    throw new Error(message)
  }

  const tokenRest = await response.json()
  return TokenMapper.toToken(tokenRest)
}
