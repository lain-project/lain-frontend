import axios from 'axios'
import {headers, neuralactionsToken} from '@/api/headers.service'
import { Propuesta } from '@/model/propuesta.model'

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/propuesta`

export const obtenerPropuesta = async (materiaId: string) => {
  try {
    // eslint-disable-next-line no-undef
    const response = await axios.get(`${URL}/${materiaId}`, {
      headers: headers()
    })
    const propuesta = response.data || new Propuesta()
    return propuesta
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const guardarPropuesta = async (propuesta: Propuesta | null) => {
  try {
    const body = {
      ...propuesta,
      ...neuralactionsToken()
    }
    // eslint-disable-next-line no-undef
    const response = await axios.post(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const actualizarPropuesta = async (propuesta: Propuesta | null) => {
  try {
    // eslint-disable-next-line no-undef
    const body = {
      ...propuesta,
      ...neuralactionsToken()
    }
    const response = await axios.put(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
