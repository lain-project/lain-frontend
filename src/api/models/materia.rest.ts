export class MateriaRest {
  id?: number
  nombre?: string
  codigo?: string
  regimen?: string | null | undefined
  anio?: number
  anio_plan_de_estudio?: number | null | undefined
  horas_semanales?: number | null | undefined
  horas_totales?: number | null | undefined
}
