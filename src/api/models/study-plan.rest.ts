export class StudyPlanRest {
  id?: number;
  code?: string;
  years?: number;
  name?: string;
  description?: string;
}
