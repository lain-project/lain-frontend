import axios from 'axios'
import { headers, neuralactionsToken } from '@/api/headers.service'
import { TipoEntidad } from '@/model/entidad.model'
import { generarTitle } from '@/api/util.service'

const URL = `${import.meta.env.VITE_APP_BASE_URL_CHATBOT}`
const URL_NEST = `${import.meta.env.VITE_APP_BASE_URL_NEST}`

export const obtenerEntidades = async (): Promise<TipoEntidad[]> => {
  try {
    const response = await axios.get<TipoEntidad[]>(`${URL}/entidades`, {
      headers: headers()
    })
    // return response.data as TipoEntidad[]
    // @ts-ignore
    return response.data.map(entidad => ({ ...entidad, title: generarTitle(entidad.displayName) }))
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const actualizarTipoEntidad = async (tipoEntidad: TipoEntidad | null): Promise<any> => {
  try {
    const response = await axios.post<TipoEntidad[]>(`${URL}/train`, tipoEntidad, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const sincronizarDatos = async (entidades: TipoEntidad[]): Promise<any> => {
  try {
    const response = await axios.post<TipoEntidad[]>(`${URL_NEST}/plaan-de-estudio/export`, {
      entidades,
      ...neuralactionsToken()
    }, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
