import axios from 'axios'
import { headers, neuralactionsToken } from '@/api/headers.service'
import { Correlativa } from '@/model/correlativa.model'
import {Materia} from "@/model/materia";

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/correlativa`

export const obtenerCorrelativas = async (idMateria: string): Promise<Correlativa[]> => {
  try {
    const response = await axios.get<Correlativa[]>(`${URL}/${idMateria}`, {
      headers: headers()
    })
    return response.data as Correlativa[]
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const obtenerMateriasCorrelativas = async (idMateria: string): Promise<Materia[]> => {
  try {
    const response = await axios.get<Correlativa[]>(`${URL}/materias/${idMateria}`, {
      headers: headers()
    })
    return response.data as Materia[]
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const asignarCorrelativa = async (correlativa: Correlativa): Promise<boolean> => {
  try {
    const body = {
      ...correlativa,
      ...neuralactionsToken()
    }
    const response = await axios.post<boolean>(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const eliminarCorrelativa = async (idCorrelativa?: string): Promise<Correlativa> => {
  try {
    const response = await axios.delete<Correlativa>(`${URL}/${idCorrelativa}`, {
      headers: headers()
    })
    return response.data as Correlativa
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
