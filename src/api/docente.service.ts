import axios from 'axios'
import { headers, neuralactionsToken } from '@/api/headers.service'
import { Docente } from '@/model/docente.model'

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/docentes`

export const getDocentesMateria = async (idMateria: string): Promise<Docente[]> => {
  try {
    const response = await axios.get(`${URL}/${idMateria}`, {
      headers: headers()
    })
    return response.data as Docente[]
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const asignarDocenteMateria = async (idMateria: string, idDocente: number): Promise<boolean> => {
  const body = {
    idUsuario: idDocente,
    ...neuralactionsToken()
  }
  try {
    const response = await axios.post(`${URL}/${idMateria}`, body, { headers: headers() })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const desasignarDocenteMateria = async (idMateria: string, idDocente: number): Promise<boolean> => {
  try {
    const response = await axios.delete(`${URL}/${idMateria}/${idDocente}`, { headers: headers() })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const obtenerDocentesNoAsignados = async (idMateria: string): Promise<Docente[]> => {
  try {
    const response = await axios.get(`${URL}/${idMateria}/no-asignados`, { headers: headers() })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
