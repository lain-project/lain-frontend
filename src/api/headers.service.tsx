export const headers = () => ({
  'Content-Type': 'application/json',
  // eslint-disable-next-line no-undef
  Authorization: `Bearer ${localStorage.getItem('token' || '')}`
})

export const neuralactionsToken = () => ({
  // eslint-disable-next-line no-undef
  neuralactionsToken: localStorage.getItem('tokenneuralaction' || '')
})
