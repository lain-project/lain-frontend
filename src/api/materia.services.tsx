import { MateriaRest } from '@/api/models/materia.rest'
import { headers } from '@/api/headers.service'
import axios from 'axios'
import { Materia } from '@/model/materia'
import { materiaAdapterToRest } from '@/adapters/materia.adapter'

const URL = `${import.meta.env.VITE_APP_BASE_URL}/subject`

export const obtenerMateriasPlanEstudio = async (idPlanEstudio: number): Promise<MateriaRest[]> => {
  try {
    // eslint-disable-next-line no-undef
    const response = await axios.get(`${URL}/${idPlanEstudio}`, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const guardarMateria = async (materia: Materia) => {
  console.log(`guardarMateria ${materia}`)
  try {
    const materiaRest = materiaAdapterToRest(materia)
    console.log(`guardarMateria ${materiaRest}`)
    // eslint-disable-next-line no-undef
    const response = await axios.post(URL, materiaRest, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

// const sleep = (milliseconds: number | undefined) => {
//   return new Promise(resolve => setTimeout(resolve, milliseconds))
// }

export const actualizarMateria = async (materia: Materia, id: string) => {
  try {
    const materiaRest = materiaAdapterToRest(materia)
    const response = await axios.put(`${URL}/${id}`, materiaRest, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
