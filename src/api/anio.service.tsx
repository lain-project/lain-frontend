import { Anio } from '@/model/anio.model'
import axios from 'axios'
import { headers } from '@/api/headers.service'

const URL = `${import.meta.env.VITE_APP_BASE_URL}/anio`

export const obtenerAniosPlanDeEstudio = async (idPlanEstudio: number): Promise<[Anio]> => {
  try {
    // eslint-disable-next-line no-undef
    const response = await axios.get(`${URL}/${idPlanEstudio}`, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
