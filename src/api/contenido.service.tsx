import axios from 'axios'
import { headers, neuralactionsToken } from '@/api/headers.service'
import { Contenido } from '@/model/contenido.model'

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/contenido`

export const obtenerContenido = async (idUnidad: string): Promise<Contenido[]> => {
  try {
    const response = await axios.get<Contenido[]>(`${URL}/${idUnidad}`, {
      headers: headers()
    })
    return response.data as Contenido[]
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const asignarContenido = async (contenido: Contenido): Promise<boolean> => {
  try {
    const body = {
      ...contenido,
      ...neuralactionsToken()
    }
    const response = await axios.post<boolean>(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const actualizarContenido = async (contenido: Contenido | null): Promise<boolean> => {
  try {
    const body = {
      ...contenido,
      ...neuralactionsToken()
    }
    const response = await axios.put<boolean>(`${URL}`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.message)
  }
}

export const eliminarContenido = async (idContenido?: string): Promise<Contenido> => {
  try {
    const response = await axios.delete<Contenido>(`${URL}/${idContenido}`, {
      headers: headers()
    })
    return response.data as Contenido
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
