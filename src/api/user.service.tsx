import { UserProfile } from '@/model/user'
import { headers } from '@/api/headers.service'

const url = `${import.meta.env.VITE_APP_BASE_URL}/users`


export const createUser = async (user: UserProfile) => {
  const data = { ...user, rol_id: user.rolId }
  const response = await fetch(`${url}`, {
    method: 'POST',
    body: JSON.stringify(data),
    headers: headers()
  })

  if (response.status === 500 || response.status === 409) {
    const { message } = await response.json()
    throw new Error(message)
  }
  return await response.json()
}

export const getUsers = async () => {
  const response = await fetch(`${url}`, {
    headers: headers()
  })

  if (response.status === 500) {
    const { message } = await response.json()
    throw new Error(message)
  }
  return await response.json()
}
