export const camelToNormalCase = (str: string) => str.replace(/[A-Z]/g, letter => ` ${letter.toLowerCase()}`)

export const generarTitle = (str: string): string => {
  const normalCase = camelToNormalCase(str)
  const result = normalCase[0].toUpperCase() + normalCase.substring(1, normalCase.length).toLowerCase()
  return result
}
