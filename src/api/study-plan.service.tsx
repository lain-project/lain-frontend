import { StudyPlanRest } from '@/api/models/study-plan.rest'
import axios from 'axios'
import { headers } from '@/api/headers.service'
import { planDeEstudioAdapter } from '@/adapters/study-plan.adapter'

const URL = `${import.meta.env.VITE_APP_BASE_URL}/study-plan`
const URL_NEST = `${import.meta.env.VITE_APP_BASE_URL_NEST}/plan-de-estudio`

export const getStudyPlans = async (): Promise<StudyPlanRest[]> => {
  try {
    // eslint-disable-next-line no-undef
    const response = await axios.get(`${URL}`, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const createStudyPlan = async (studyPlan: StudyPlanRest): Promise<StudyPlanRest> => {
  try {
    const withYears = { ...studyPlan, years: 5 }
    const response = await axios.post(`${URL}`, withYears, { headers: headers() })
    console.warn(response)
    return response.data
  } catch (error: any) {
    const { response } = error
    if (response.status === 409) {
      // eslint-disable-next-line no-throw-literal
      throw 'El código ya se encuentra en uso'
    }
    // eslint-disable-next-line no-throw-literal
    throw 'Ocurrio un error en el servidor'
  }
}

export const updateStudyPlan = async (studyPlan: StudyPlanRest): Promise<StudyPlanRest> => {
  try {
    const response = await axios.put(`${URL}/${studyPlan.id}`, studyPlan, { headers: headers() })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const obtenerPlanesDeEstudioAsignados = async (idUsuario: number | undefined): Promise<StudyPlanRest[]> => {
  try {
    const response = await axios.get(`${URL_NEST}/${idUsuario}`, { headers: headers() })
    const planes = response.data.map((plan: any) => planDeEstudioAdapter(plan))
    return planes
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
