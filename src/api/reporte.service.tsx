import axios from 'axios'
import { headers } from '@/api/headers.service'

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/reportes`

export const obtenerTotalPreguntas = async (fechas: any): Promise<any> => {
  console.log(fechas)
  try {
    const response = await axios.post<any>(`${URL}/total-preguntas`, fechas, {
      headers: headers()
    })
    // @ts-ignore
    return response.data as any
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
