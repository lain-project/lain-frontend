import { Unidad } from '@/model/unidad.model'
import axios from 'axios'
import { headers, neuralactionsToken } from '@/api/headers.service'

const URL = `${import.meta.env.VITE_APP_BASE_URL_NEST}/unidad`

export const obtenerUnidades = async (idMateria: string): Promise<Unidad[]> => {
  try {
    const response = await axios.get<Unidad[]>(`${URL}/${idMateria}`, {
      headers: headers()
    })
    return response.data as Unidad[]
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const asignarUnidad = async (unidad: Unidad): Promise<boolean> => {
  try {
    const body = {
      ...unidad,
      ...neuralactionsToken()
    }
    const response = await axios.post<boolean>(`${URL}/`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}

export const actualizarUnidad = async (unidad: Unidad): Promise<boolean> => {
  try {
    const body = {
      ...unidad,
      ...neuralactionsToken()
    }
    const response = await axios.put<boolean>(`${URL}/`, body, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.message)
  }
}

export const desasignarUnidad = async (idUnidad?: string): Promise<Unidad> => {
  try {
    const response = await axios.delete<Unidad>(`${URL}/${idUnidad}`, {
      headers: headers()
    })
    return response.data
  } catch (error: any) {
    throw new Error(error.response.data.detail)
  }
}
