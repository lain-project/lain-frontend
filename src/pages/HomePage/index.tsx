import { useStore } from '@/zustand/store'

export function HomePage () {
  const { user } = useStore()
  return (
        <div className={'card text-center'}>
          <h1>Bienvenido {user.username} al sistema</h1>
          <h2>Tienes asignado el rol de {user.rolname?.toLowerCase()}</h2>
        </div>
  )
}
