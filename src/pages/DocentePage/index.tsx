import { useNavigate } from 'react-router-dom'
import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { Button } from 'primereact/button'
import { Docente } from '@/model/docente.model'
import { useStoreDocente } from '@/zustand/store/docente.store'
import { DocenteTable } from '@/pages/DocentePage/components/DocenteTable'
import { useStoreMaterias } from '@/zustand/store'
import { ConfirmDialog } from 'primereact/confirmdialog'
import { desasignarDocenteMateria, getDocentesMateria } from '@/api/docente.service'
import { DocentesDialog } from '@/pages/DocentePage/components/DocentesDialog'
import { Spiner } from '@/components/Spiner'
import { useRol } from '@/hooks/useRol.hook'
import { ButtonNew } from '@/components/ButtonNew'
import { ButtonBack } from '@/components/ButtonBack'

export const DocentePage = () => {
  const toast = useRef<Toast>(null)
  const { getDocentesNoAsignados, setDocentes } = useStoreDocente()
  const { materiaSeleccionada } = useStoreMaterias()
  const [mostrarDialog, setMostrarDialog] = useState(false)
  const [mostrarDeleteDialog, setMostrarDeleteDialog] = useState(false)
  const { docentesMateria, getDocentes } = useStoreDocente()
  const navigate = useNavigate()
  const [docenteEliminar, setDocenteEliminar] = useState<Docente | null>(null)
  const [loading, setLoading] = useState(false)
  const { isAdmin } = useRol()

  useEffect(() => {
    getDocentes(materiaSeleccionada.id)
  }, [])

  const openNew = async () => {
    await getDocentesNoAsignados(materiaSeleccionada.id)
    setMostrarDialog(true)
  }

  const closeDocenteDialog = async () => {
    const docentes = await getDocentesMateria(materiaSeleccionada.id)
    setDocentes(docentes)
    setMostrarDialog(false)
  }

  const eliminarDocente = async () => {
    const docenteId = docenteEliminar?.id || 0
    setLoading(true)
    await desasignarDocenteMateria(materiaSeleccionada.id, docenteId)
    await getDocentes(materiaSeleccionada.id)
    setDocenteEliminar(null)
    setLoading(false)
    setMostrarDeleteDialog(false)
  }

  const nuevo = (isAdmin() && <ButtonNew onClick={async () => await openNew()}/>)
  const volver = (<ButtonBack />)

  const Footer = (
    (!loading
      ? <div>
          <Button label="Cancelar"
                  icon="pi pi-times"
                  className="p-button-danger"
                  onClick={() => setMostrarDeleteDialog(false)}/>
          <Button label={'Guardar'}
                  icon="pi pi-save"
                  className="p-button-success"
                  onClick={async () => await eliminarDocente()}
          />
      </div>
      : <div></div>)
  )

  const loadingDialogProps = {
    message: (<Spiner/>),
    header: 'Guardando...',
    closable: false

  }
  const messageConfirm = {
    message: '¿Desea desasignar al docente de la materia?',
    header: 'Confirmar eliminación',
    closable: false
  }
  return (
    <>
      {docentesMateria && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" left={volver} right={nuevo}></Toolbar>
              <ConfirmDialog
                visible={mostrarDeleteDialog}
                footer={Footer}
                style={{ width: '30rem' }}
                {...(loading ? loadingDialogProps : messageConfirm)} />
               <DocentesDialog mostrarDialog={mostrarDialog} onClose={async () => closeDocenteDialog()} />
            </div>
            <DocenteTable
              openDialog={() => setMostrarDeleteDialog(true)}
              setDocenteEliminar={setDocenteEliminar}
            />
          </div>
        </div>)
      }
    </>
  )
}
