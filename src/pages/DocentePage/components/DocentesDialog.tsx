import { FunctionComponent, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { Dialog } from 'primereact/dialog'
import { Button } from 'primereact/button'
import { useStoreDocente } from '@/zustand/store/docente.store'
import { Table } from '@/components/Table/Table'
import { Column } from 'primereact/column'
import { DataTable } from 'primereact/datatable'
import { Docente } from '@/model/docente.model'
import { asignarDocenteMateria } from '@/api/docente.service'
import { useStoreMaterias } from '@/zustand/store'
import {Spiner} from "@/components/Spiner";

export const DocentesDialog: FunctionComponent<any> = ({ mostrarDialog, onClose }: any) => {
  const { docentesNoAsignadosMateria, loading, setLoading } = useStoreDocente()
  const { materiaSeleccionada } = useStoreMaterias()
  const toast = useRef<Toast>(null)
  const tableRef = useRef<DataTable>(null)
  const [docentesAsignar, setDocentesAsignar] = useState<Docente[]>([])

  const ocultarDialog = async () => {
    onClose()
  }

  const asignarDocentesSeleccionados = async () => {
    setLoading(true)
    // @ts-ignore
    const promises = docentesAsignar.map(docente => asignarDocenteMateria(materiaSeleccionada.id, docente.id))
    await Promise.all(promises)
    await onClose()
    setLoading(false)
  }

  const dialogFooter = (
    <>
      <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={onClose}/>
      <Button label={'Guardar'}
              icon="pi pi-check"
              className="p-button-text"
              onClick={ asignarDocentesSeleccionados}/>
    </>
  )

  const dialogProperties = {
    header: 'Seleccione docente a asignar',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: dialogFooter

  }
  const loadingDialogProps = {
    header: 'Guardando...'
  }

  return (
    <>
      <Toast ref={toast}/>
      <Dialog visible={mostrarDialog}
              style={{ width: '60rem' }}
              closable={false}
              className='p-fluid'
              onHide={() => ocultarDialog()}
              {...(loading ? loadingDialogProps : dialogProperties)}>
        <>
          {(loading) && <Spiner/>}
          {(!loading) && (
            <Table tableRef={tableRef}
                   data={docentesNoAsignadosMateria}
                   emptyMessage={'No hay docente para asignar'}
                   onSelectRow={(docentes: Docente[]) => setDocentesAsignar(docentes)}
                   selection={true}
                   loading={loading}>
              <Column field="username" header="Username" sortable></Column>
              <Column field="email" header="Email" sortable></Column>
              <Column field="perfil.apellidos" header="Apellidos" sortable></Column>
              <Column field="perfil.nombres" header="Nombres" sortable></Column>
            </Table>)}
        </>
      </Dialog>
    </>
  )
}
