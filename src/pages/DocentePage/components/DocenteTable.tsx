import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { useRef } from 'react'
import { Table } from '@/components/Table/Table'
import { useStoreDocente } from '../../../zustand/store/docente.store'
import { useStoreMaterias } from '@/zustand/store'
import { Button } from 'primereact/button'
import { Docente } from '@/model/docente.model'
import { useRol } from '@/hooks/useRol.hook'

export const DocenteTable = ({ openDialog, setDocenteEliminar }: any) => {
  const { docentesMateria, loading } = useStoreDocente()
  const { materiaSeleccionada } = useStoreMaterias()
  const { isAdmin } = useRol()

  const tableRef = useRef<DataTable>(null)

  const actionBodyTemplate = (docente: Docente) => (
    <>
      <Button
        icon="pi pi-trash"
        className="p-button-rounded p-button-danger mr-2"
        tooltip={'Desasignar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setDocenteEliminar(docente)
          openDialog()
        }}
      />
    </>
  )

  return (
    <>
      {(loading) && <div>Loading...</div>}
      {(!loading) && (
        <Table tableRef={tableRef}
               data={docentesMateria}
               title={`Docentes de la materia ${materiaSeleccionada.nombre}`}
               emptyMessage={'No se asigno ningun docente hasta el momento'}
               loading={loading}>
          <Column field="username" header="Username" sortable></Column>
          <Column field="email" header="Email" sortable></Column>
          <Column field="perfil.apellidos" header="Apellidos" sortable></Column>
          <Column field="perfil.nombres" header="Nombres" sortable></Column>
          {isAdmin() && <Column body={actionBodyTemplate}></Column> }
        </Table>)}
    </>
  )
}
