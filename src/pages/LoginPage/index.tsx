import './style.scss'
import { LoginButton } from '@/components/LoginButton'
import { Toast } from 'primereact/toast'
import { Badge } from 'primereact/badge'
import { Card } from 'primereact/card'
import { Divider } from 'primereact/divider'
import { GlassInput } from '@/components/GlassInput'
import { useForm } from '@/hooks/useForm'
import { useRef, useState } from 'react'
import { User, UserStore } from '@/model/user'
import { googleLogin, login } from '@/api/auth.service'
import { useNavigate } from 'react-router-dom'
import { useStore } from '@/zustand/store'
import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login'
import { Token } from '@/model/token'
import jwtDecode from 'jwt-decode'
import { ParticleBackground } from '@/components/ParticleBackground'

const env = import.meta.env

export const LoginPage = () => {
  const navigate = useNavigate()
  const toastRef = useRef<any>(null)
  const { setUser, setToken } = useStore()
  const [errorUsername, setErrorUsername] = useState(false)
  const [errorPassword, setErrorPassword] = useState(false)
  const [formValues, handleInputChange] = useForm({
    username: '',
    password: ''
  })

  const clientId = env.VITE_GOOGLE_CLIENT_ID ?? ''
  const { username, password } = formValues

  const showToastError = (summary: string, detail: string) => {
    if (toastRef.current !== null) {
      toastRef.current.show({ severity: 'error', summary, detail, life: 3000 })
    }
  }

  const handleGoogleLoginFailure = (error: any) => {
    if (error.error === 'popup_closed_by_user') {
      return
    }
    const message = (error as Error).message
    showToastError('Error', message)
  }
  const onLoadFailure = (error: any) => {
    showToastError('Error', error.details)
  }

  const validaNotEmpty = (value: string) => {
    return value.trim().length === 0
  }

  const saveTokenAndRedirect = async (token: Token) => {
    // eslint-disable-next-line no-undef
    localStorage.setItem('token', token.accessToken)
    // eslint-disable-next-line no-undef
    localStorage.setItem('tokenneuralaction', token.tokenNeuralaction)
    const tokenData = await jwtDecode(token.accessToken) as any
    const userData = tokenData.user as UserStore
    userData.logged = true
    setUser(userData)
    setToken(token)
  }

  const handleSubmit = async (e: any) => {
    e.preventDefault()
    const errorUsername = validaNotEmpty(username)
    const errorPassword = validaNotEmpty(password)

    setErrorUsername(errorUsername)
    setErrorPassword(errorPassword)

    if (!errorUsername && !errorPassword) {
      const user = new User()
      user.username = username
      user.password = password
      try {
        const token = await login(user)
        await saveTokenAndRedirect(token)
      } catch (error) {
        const message = (error as Error).message
        showToastError('Error', message)
      }
    }
  }

  const handleCredentialResponse = async (res: GoogleLoginResponse | GoogleLoginResponseOffline) => {
    const response = res as GoogleLoginResponse

    if (response.code) {
      showToastError('Error', 'Error en validacion de google')
      return
    }

    const { tokenId } = response
    try {
      const token = await googleLogin(tokenId)
      await saveTokenAndRedirect(token)
    } catch (error) {
      const message = (error as Error).message
      showToastError('Error', message)
    }
  }
  // testing

  return (
    <>
      <Toast ref={toastRef}/>
      <ParticleBackground/>
      <form className='login-container toast-demo' onSubmit={handleSubmit}>
        <Card className='login-card'>
          <div className='login-card-content'>
            <h1>Sign In</h1>
            <GlassInput
              title="Username"
              type="text"
              id="username"
              errorMessage="Username vacio"
              onChange={handleInputChange}
              value={username}
              name="username"
              error={errorUsername}
            />
            <GlassInput
              title="Password"
              type="password"
              id="password"
              errorMessage="Password vacio"
              onChange={handleInputChange}
              value={password}
              name="password"
              error={errorPassword}
            />

            <LoginButton className="w-100" label="Log In" type='submit'/>
            <div className="w-100">
              <Divider align="center">
                <Badge value="O" className='bg'></Badge>
              </Divider>
            </div>

            <GoogleLogin
              cookiePolicy='single_host_origin'
              clientId={clientId}
              onSuccess={handleCredentialResponse}
              theme='light'
              onFailure={handleGoogleLoginFailure}
              onScriptLoadFailure={onLoadFailure}
              prompt="select_account"
            />
          </div>
        </Card>
      </form>
    </>
  )
}
