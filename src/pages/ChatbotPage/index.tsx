import { useStoreChatbot } from '@/zustand/store/chatbot.store'
import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { BibliografiaDialog } from '@/pages/BibliografiaPage/components/BibliografiaDialog'
import { ConfirmDialog } from 'primereact/confirmdialog'
import { BibliografiaTable } from '@/pages/BibliografiaPage/components/BibliografiaTable'
import { ChatbotTable } from '@/pages/ChatbotPage/components/ChatbotTable'
import { Button } from 'primereact/button'
import { actualizarTipoEntidad, sincronizarDatos } from '@/api/chatbot.service'

export const ChatbotPage = () => {
  const toast = useRef<Toast>(null)
  const { obtenerEntidades, entidades, updateLoading } = useStoreChatbot()
  const [disabled, setDisabled] = useState(false)

  useEffect(() => {
    obtenerEntidades()
  }, [])

  const rigthToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Sincronizar"
                  disabled={disabled}
                  icon="pi pi-plus" className="p-button-success mr-2" onClick={async () => {
                    updateLoading(true)
                    setDisabled(true)
                    try {
                      const result = await sincronizarDatos(entidades)
                      await obtenerEntidades()
                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                      // @ts-ignore
                      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Sincronización exitosa', life: 3000 })
                    } catch (error) {
                      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                      // @ts-ignore
                      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: 'Error al sincronizar los datos, intente mas tarde', life: 3000 })
                    }
                    updateLoading(false)
                    setDisabled(false)
                  }}/>
        </div>
      </>
    )
  }

  return (
    <>
      {entidades && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toolbar className="mb-4" right={rigthToolbarTemplate}></Toolbar>
              <Toast ref={toast}/>
            </div>
            <ChatbotTable />
          </div>
        </div>)}
    </>
  )
}
