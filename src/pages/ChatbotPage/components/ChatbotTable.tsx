import { Spiner } from '@/components/Spiner'
import { Table } from '@/components/Table/Table'
import { Column } from 'primereact/column'
import { useStoreChatbot } from '@/zustand/store/chatbot.store'
import { useRef } from 'react'
import { DataTable } from 'primereact/datatable'
import { Button } from 'primereact/button'
import { TipoEntidad } from '@/model/entidad.model'
import { useNavigate } from 'react-router-dom'

export const ChatbotTable = () => {
  const { loading, entidades, setEntidadSeleccionada } = useStoreChatbot()
  const tableRef = useRef<DataTable>(null)
  const navigate = useNavigate()

  const actionBodyTemplate = (entidad: TipoEntidad) => (
    <>
      <Button
        icon="pi pi-pencil"
        className="p-button-rounded p-button-success mr-2"
        tooltip={'Editar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setEntidadSeleccionada(entidad)
          navigate(`${entidad.displayName}`)
        }}
      />
    </>
  )

  return (
    <>
      {loading && <Spiner/>}
      {!loading && (
        <Table tableRef={tableRef}
               data={entidades}
               title={'Entidades del chatbot'}
               emptyMessage={'No se agrego ninguna entidad'}
               loading={loading}>
          <Column field="title" header="Nombre" sortable></Column>
          <Column body={actionBodyTemplate}></Column>
        </Table>
      )}
    </>
  )
}
