import { Spiner } from '@/components/Spiner'
import { Table } from '@/components/Table/Table'
import { Column } from 'primereact/column'
import { useStoreChatbot } from '@/zustand/store/chatbot.store'
import { useEffect, useRef, useState } from 'react'
import { DataTable } from 'primereact/datatable'
import { useParams } from 'react-router-dom'
import { Entidad } from '@/model/entidad.model'
import { Chips } from 'primereact/chips'
import { Toolbar } from 'primereact/toolbar'
import { Button } from 'primereact/button'
import { useNavigation } from '@/hooks/useNavigation.hook'
import { actualizarTipoEntidad } from '@/api/chatbot.service'

export const EntidadPage = () => {
  const { loading, entidades } = useStoreChatbot()
  const tableRef = useRef<DataTable>(null)
  const { id } = useParams()
  const { volver } = useNavigation()
  const [tipoEntidad, setTipoEntidad] = useState(null)
  const [entities, setEntities] = useState([])

  useEffect(() => {
    const tipoEntidad = entidades.find(entidad => entidad.displayName === id)
    setTipoEntidad(tipoEntidad)
    setEntities(tipoEntidad.entities)
  }, [])

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Volver" icon="pi pi-arrow-left" className="p-button-danger mr-2" onClick={volver}/>
        </div>
      </>
    )
  }

  const rigthToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Actualizar" icon="pi pi-plus" className="p-button-success mr-2" onClick={async () => {
            const result = await actualizarTipoEntidad(tipoEntidad)
            volver()
          }}/>
        </div>
      </>
    )
  }

  const synonymsTemplate = (entity: Entidad) => {
    const id = entity.value
    const synonym = entity.synonyms
    console.log(entity)
    return (
      <>
        <Chips value={synonym} onChange={({ value }) => {
          const e = entities.findIndex(entidad => entidad?.value === id)
          const nuevaEntidad = {
            synonyms: value,
            value: id
          }
          const result = entities.map((entidad, index) => index === e ? nuevaEntidad : entidad)
          setEntities(result)
          const tentidad = { ...tipoEntidad }
          tentidad.entities = result
          setTipoEntidad(tentidad)
        }} />
      </>
    )
  }

  return (
    <>
      {loading && <Spiner/>}
      {!loading && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toolbar className="mb-4" left={leftToolbarTemplate} right={rigthToolbarTemplate}></Toolbar>
            </div>
              <Table tableRef={tableRef}
               data={entities}
               title={`${tipoEntidad?.title} en bot`}
               emptyMessage={'No se agrego ninguna entidad'}
               loading={loading}>
          <Column field="value" header="Nombre" sortable></Column>
          <Column field="synonyms" header="Sinonimos" body={synonymsTemplate} sortable></Column>
          {/* <Column body={actionBodyTemplate}></Column> */}
        </Table>
          </div>
        </div>
      )}
    </>
  )
}
