import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { useNavigation } from '@/hooks/useNavigation.hook'
import { Button } from 'primereact/button'
import { Spiner } from '@/components/Spiner'
import { Toolbar } from 'primereact/toolbar'
import { ConfirmDialog } from 'primereact/confirmdialog'
import { useStoreMaterias } from '@/zustand/store'
import { useStoreBibliografia } from '@/zustand/store/bibliografia.store'
import { BibliografiaTable } from '@/pages/BibliografiaPage/components/BibliografiaTable'
import { BibliografiaDialog } from '@/pages/BibliografiaPage/components/BibliografiaDialog'
import { eliminarBibliograia } from '@/api/bibliografia.service'

export const BibliografiaPage = () => {
  const toast = useRef<Toast>(null)
  const { volver } = useNavigation()
  const [mostrarDialog, setMostrarDialog] = useState(false)
  const [mostrarDialogDelete, setMostrarDialogDelete] = useState(false)
  const { materiaSeleccionada } = useStoreMaterias()
  const { getBibliografia, bibliografiaMateria, bibliografiaSeleccionada, setBibliografiaSeleccionada } = useStoreBibliografia()
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    getBibliografia(materiaSeleccionada.id)
  }, [])

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Volver" icon="pi pi-arrow-left" className="p-button-danger mr-2" onClick={volver}/>
        </div>
      </>
    )
  }

  const rigthToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Nuevo" icon="pi pi-plus" className="p-button-success mr-2" onClick={() => setMostrarDialog(true)}/>
        </div>
      </>
    )
  }

  const eliminar = async () => {
    const idBibliografia = bibliografiaSeleccionada?.id
    setLoading(true)
    await eliminarBibliograia(idBibliografia)
    await getBibliografia(materiaSeleccionada.id)
    setBibliografiaSeleccionada(null)
    setMostrarDialogDelete(false)
    setLoading(false)
  }

  const Footer = (
    (!loading
      ? <div>
        <Button label="Cancelar"
                icon="pi pi-times"
                className="p-button-danger"
                onClick={() => setMostrarDialogDelete(false)}/>
        <Button label={'Guardar'}
                icon="pi pi-save"
                className="p-button-success"
                onClick={async () => await eliminar()}
        />
      </div>
      : <div></div>)
  )

  const loadingDialogProps = {
    message: (<Spiner/>),
    header: 'Guardando...',
    closable: false

  }
  const messageConfirm = {
    message: '¿Desea eliminar la bibliografia?',
    header: 'Confirmar eliminación',
    closable: false
  }

  return (
    <>
      {bibliografiaMateria && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" left={leftToolbarTemplate} right={rigthToolbarTemplate}></Toolbar>
              <BibliografiaDialog mostrarDialog={mostrarDialog} cerrarDialog={() => setMostrarDialog(false)}/>
              <ConfirmDialog
                visible={mostrarDialogDelete}
                footer={Footer}
                style={{ width: '30rem' }}
                {...(loading ? loadingDialogProps : messageConfirm)}
              />

            </div>
            <BibliografiaTable
              openDialog={() => setMostrarDialog(true)}
              openDeleteBibliografia={() => setMostrarDialogDelete(true)}
            />
          </div>
        </div>)}
    </>
  )
}
