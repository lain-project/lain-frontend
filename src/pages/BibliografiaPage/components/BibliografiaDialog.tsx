import { FunctionComponent, useEffect, useRef, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useStoreMaterias } from '@/zustand/store'
import { Button } from 'primereact/button'
import { Toast } from 'primereact/toast'
import { Dialog } from 'primereact/dialog'
import { InputText } from 'primereact/inputtext'
import { Bibliografia } from '@/model/bibliografia.model'
import { useStoreBibliografia } from '@/zustand/store/bibliografia.store'
import { actualizarBibliografia, asignarBibliografia } from '@/api/bibliografia.service'

export const BibliografiaDialog: FunctionComponent<any> = ({ mostrarDialog, cerrarDialog }: any) => {
  const toast = useRef<any>(null)
  const { register, handleSubmit, reset, formState: { errors } } = useForm<Bibliografia>()
  const { materiaSeleccionada } = useStoreMaterias()
  const { setToUpdate, setBibliografiaSeleccionada, bibliografiaAActualizar, getBibliografia } = useStoreBibliografia()
  const [isUpdated, setIsUpdated] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (bibliografiaAActualizar) {
      reset({ ...bibliografiaAActualizar })
      setIsUpdated(true)
      return
    }
    reset({ ...register })
    setIsUpdated(false)
  }, [mostrarDialog, cerrarDialog])

  const guardarBibliografia = async (bibliografia: Bibliografia) => {
    bibliografia.materiaId = materiaSeleccionada.id
    try {
      setLoading(true)
      if (isUpdated) {
        await actualizarBibliografia(bibliografia)
      } else {
        await asignarBibliografia(bibliografia)
      }
      setToUpdate(null)
      reset({ ...register })
      getBibliografia(materiaSeleccionada.id)
      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Bibliografia creada exitosamente', life: 3000 })
      setLoading(false)
      cerrarDialog()
    } catch (error: any) {
      setLoading(false)
      const message = (error as Error).message
      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: message, life: 3000 })
    }
  }

  const dialogFooter = (
    <>
      <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={() => {
        setToUpdate(null)
        reset({ ...register })
        cerrarDialog()
      }}/>
      <Button label={ !isUpdated ? 'Guardar' : 'Actualizar'}
              icon="pi pi-check"
              className="p-button-text"
              onClick={handleSubmit(guardarBibliografia)}/>
    </>
  )

  const dialogProps = {
    header: (isUpdated ? 'Actualizar ' : 'Crear ') + 'bibliografia',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: dialogFooter
  }

  const loadingDialogProps = {
    header: 'Guardando...'
  }

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  return (
    <>
      <Toast ref={toast} />
      <Dialog visible={mostrarDialog}
              style={{ width: '450px' }}
              closable={false}
              className='p-fluid'
              onHide={cerrarDialog}
              {...(loading ? loadingDialogProps : dialogProps)}>
        {loading && <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}></i>}
        <div className={loading ? 'blur' : ''}>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="Nombre" {...register('nombre', { required: 'El nombre es obligatorio' })}/>
            </span>
            {getFormErrorMessage('nombre')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="ISBN"
                         {...register('isbn', { required: 'El isbn es obligatorio' })}/>
            </span>
            {getFormErrorMessage('isbn')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="Autor"
                         {...register('autor')}/>
            </span>
          </div>
        </div>
      </Dialog>
    </>
  )
}
