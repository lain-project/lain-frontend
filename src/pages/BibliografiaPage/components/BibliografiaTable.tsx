import { useStoreMaterias } from '@/zustand/store'
import { useRef } from 'react'
import { DataTable } from 'primereact/datatable'
import { Button } from 'primereact/button'
import { Spiner } from '@/components/Spiner'
import { Table } from '@/components/Table/Table'
import { Column } from 'primereact/column'
import { useStoreBibliografia } from '@/zustand/store/bibliografia.store'
import { Bibliografia } from '@/model/bibliografia.model'

export const BibliografiaTable = ({ openDialog, openDeleteBibliografia }: any) => {
  const { bibliografiaMateria, loading, setToUpdate, setBibliografiaSeleccionada } = useStoreBibliografia()
  const { materiaSeleccionada } = useStoreMaterias()

  const tableRef = useRef<DataTable>(null)

  const actionBodyTemplate = (bibliografia: Bibliografia) => (
    <>
      <Button
        icon="pi pi-pencil"
        className="p-button-rounded p-button-success mr-2"
        tooltip={'Editar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setToUpdate(bibliografia)
          openDialog()
        }}
      />
      <Button
        icon="pi pi-trash"
        className="p-button-rounded p-button-danger mr-2"
        tooltip={'Eliminar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setBibliografiaSeleccionada(bibliografia)
          openDeleteBibliografia()
        }}
      />
    </>
  )

  return (
    <>
      { loading && <Spiner/> }
      { !loading && (
        <Table tableRef={tableRef}
               data={bibliografiaMateria}
               title={`Bibliografia de la materia ${materiaSeleccionada.nombre}`}
               emptyMessage={'No se agrego ninguna bibliografia a la materia'}
               loading={loading}>
          <Column field="isbn" header="ISBN" sortable></Column>
          <Column field="nombre" header="Nombre" sortable></Column>
          <Column field="autor" header="Autor" sortable></Column>
          <Column body={actionBodyTemplate}></Column>
        </Table>
      )}
    </>
  )
}
