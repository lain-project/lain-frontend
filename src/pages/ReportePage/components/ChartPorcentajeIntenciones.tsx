import { forwardRef, useEffect, useState } from 'react'
import { Chart } from 'primereact/chart'
import { generarTitle } from '@/api/util.service'
import ChartDataLabels from 'chartjs-plugin-datalabels'
import { colors } from '@/model/color.model'

export const ChartPorcentajeIntenciones = forwardRef(({ intencionesContadas }: any, ref) => {
  const [datosGrafico, setdatosGrafico] = useState({})
  const [opciones, setOpciones] = useState({})

  const COLOR_DATA_LABEL = '#201f1f'

  const calculoPorcentaje = (valor: number, categorias: any) => {
    const datos = categorias.chart.data.datasets[0].data
    // eslint-disable-next-line no-return-assign
    const suma = datos.reduce((acc: number, prev:number) => acc += prev)
    const percentage = (valor * 100 / suma).toFixed(2) + '%'
    return percentage
  }
  const generateTitle = () => ({
    display: true,
    font: {
      size: 20
    },
    text: 'Porcentaje de intenciones'
  })
  const generateLegend = () => ({
    position: 'bottom',
    fullSize: true
  })

  const bgColor = {
    id: 'bgColor',
    beforeDraw: (chart: any, options: any) => {
      const { ctx, width, height } = chart
      ctx.fillStyle = options.backgroundColor
      ctx.fillRect(0, 0, width, height)
      ctx.restore()
    }
  }
  const generateOptions = () => ({
    plugins: {
      datalabels: {
        color: COLOR_DATA_LABEL,
        font: {
          size: 12,
          weight: 'bold'
        },
        formatter: calculoPorcentaje
      },
      title: generateTitle(),
      legend: generateLegend(),
      bgColor: {
        backgroundColor: 'white'
      }
    }
  })
  const generarDatosGrafico = (labelsTotal: any) => ({
    labels: Object.keys(labelsTotal),
    datasets: [
      {
        data: Object.values(labelsTotal),
        backgroundColor: colors,
        hoverBackgroundColor: '#CBD5E1',
        hoverOffset: 10
      }]

  })

  useEffect(() => {
    const labelsTotal = {}
    const intencionesKeys = Object.keys(intencionesContadas)

    intencionesKeys.forEach(intencion => {
      const intencionTitle = generarTitle(intencion)
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      labelsTotal[intencionTitle] = intencionesContadas[intencion]
    })
    setOpciones(generateOptions())
    setdatosGrafico(generarDatosGrafico(labelsTotal))
  }, [intencionesContadas])

  return (
      <Chart type="doughnut" data={datosGrafico} options={opciones}
             className="card"
             ref={ref}
             id="pdfsample1"
             plugins={[ChartDataLabels]} style={{ position: 'relative', width: '100%' }} />

  )
})
