import WordCloud from 'react-d3-cloud'
import {colors} from "@/model/color.model";

export const WordCloudCustom = ({data}) => {

  const schemeCategory10ScaleOrdinal = (i) => colors[i]
  const cloud = [
    { text: 'escogo ing en sistemas', value: 3 },
    { text: 'calculo', value: 6 },
    { text: 'unidades', value: 4 },
    { text: 'profes', value: 6 },
    { text: 'correlativa', value: 1 },
    { text: 'hola', value: 1 },
    { text: 'profesores', value: 3 },
    { text: 'comedor', value: 6 },
    { text: 'data', value: 4 },
    { text: 'ejemplo', value: 6 },
  ]

  return (<WordCloud
    data={cloud}
    width={500}
    height={500}
    font="Roboto"
    fontStyle="italic"
    fontWeight="bold"
    fontSize={(word) => Math.log2(word.value) * 20}
    padding={5}
    fill={(d, i) => schemeCategory10ScaleOrdinal(i)}
    onWordClick={(event, d) => {
      console.log(`onWordClick: ${d.text}`)
    }}
    onWordMouseOver={(event, d) => {
      console.log(`onWordMouseOver: ${d.text}`)
    }}
    onWordMouseOut={(event, d) => {
      console.log(`onWordMouseOut: ${d.text}`)
    }}
  />)
}
