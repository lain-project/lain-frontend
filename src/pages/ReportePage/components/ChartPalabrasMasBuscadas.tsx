import { Chart } from 'primereact/chart'
import { useEffect, useState } from 'react'
import { colors } from '@/model/color.model'
import ChartDataLabels from 'chartjs-plugin-datalabels'

export const ChartPalabrasMasBuscadas = ({ palabrasRepeticion }: any) => {
  const [datosGrafico, setDatosGrafico] = useState({})

  useEffect(() => {
    const palabraCantidad = Object.keys(palabrasRepeticion).map((palabra: string) => ({
      palabra,
      cantidad: palabrasRepeticion[palabra]
    })).sort((a, b) => b.cantidad - a.cantidad)
    const labels = palabraCantidad.map(({ palabra }) => palabra)
    const dataset = palabraCantidad.map(({ cantidad }) => cantidad)
    setDatosGrafico({
      labels,
      datasets: [{
        backgroundColor: colors,
        data: dataset,
        datalabels: {
          anchor: 'center',
          align: 'center'
        }
      }]
    }
    )
  }, [palabrasRepeticion])
  const horizontalOptions = {
    indexAxis: 'y',
    maintainAspectRatio: false,
    aspectRatio: 0.8,
    plugins: {
      datalabels: {
        color: '#201f1f',
        display: function (context: any) {
          return context.dataset.data[context.dataIndex]
        },
        font: {
          weight: 'bold'
        },
        formatter: Math.round
      },
      legend: {
        display: false
      },
      title: {
        display: true,
        font: {
          size: 20
        },
        text: 'Preguntas mas buscadas'
      }
    }
  }
  return (
    <>
      <Chart type="bar" id='pdfsample2' data={datosGrafico} options={horizontalOptions} plugins={[ChartDataLabels]} className="card"/>
    </>
  )
}
