import { Chart } from 'primereact/chart'
import { useEffect, useState } from 'react'
import { colors } from '@/model/color.model'
import ChartDataLabels from 'chartjs-plugin-datalabels'
export const ChartPalabrasPorFecha = ({ palabrasPorFecha } : any) => {
  const [datosGrafico, setDatosGrafico] = useState({})

  const obtenerPalabras = (palabrasFecha: any) => palabrasFecha.flatMap((data: any) => Object.keys(data.palabras))
  const eliminarDuplicados = (palabrasFecha: any) => new Set(obtenerPalabras(palabrasFecha))
  const obtenerDatasetLabels = (palabrasFecha: any) => Array.from(eliminarDuplicados(palabrasFecha))

  useEffect(() => {
    const labels = palabrasPorFecha.map(({ fecha }: any) => fecha)
    const datasetLabels = obtenerDatasetLabels(palabrasPorFecha)

    const datasets = datasetLabels.map((label: any, index) => ({
      label,
      data: palabrasPorFecha.map((palabraFecha:any) => palabraFecha.palabras[label] || 0),
      type: 'bar',
      backgroundColor: colors[index],
      datalabels: {
        anchor: 'center',
        align: 'center'
      }
    }))
    console.log(datasets)

    setDatosGrafico({
      labels,
      datasets
    })
  }, [palabrasPorFecha])

  const stackedOptions = {
    maintainAspectRatio: false,
    aspectRatio: 0.8,
    plugins: {
      tooltips: {
        mode: 'index',
        intersect: false
      },
      legend: {
        position: 'bottom',
        fullSize: true
      },
      title: {
        display: true,
        font: {
          size: 20
        },
        text: 'Preguntas por fecha'
      },
      datalabels: {
        color: '#201f1f',
        display: function (context: any) {
          return context.dataset.data[context.dataIndex]
        },
        font: {
          weight: 'bold'
        },
        formatter: Math.round
      }
    },
    scales: {
      x: {
        stacked: true,
        ticks: {
          color: '#495057'
        }
      },
      y: {
        stacked: true,
        ticks: {
          color: '#495057'
        }
      }
    }
  }

  return (
    <Chart type="bar" data={datosGrafico} plugins={[ChartDataLabels]} options={stackedOptions} className="card"/>
  )
}
