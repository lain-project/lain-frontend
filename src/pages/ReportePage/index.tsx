import { ChartPorcentajeIntenciones } from './components/ChartPorcentajeIntenciones'
import { Toolbar } from 'primereact/toolbar'
import { Calendar } from 'primereact/calendar'
import { Button } from 'primereact/button'
import { useCallback, useRef, useState } from 'react'
import { Dropdown } from 'primereact/dropdown'
import { obtenerTotalPreguntas } from '@/api/reporte.service'
import { ChartPalabrasPorFecha } from '@/pages/ReportePage/components/ChartPalabrasPorFecha'
import { ChartPalabrasMasBuscadas } from '@/pages/ReportePage/components/ChartPalabrasMasBuscadas'
import html2canvas from 'html2canvas'
import jsPDF from 'jspdf'
import { Img, PdfMakeWrapper, Table, Txt } from 'pdfmake-wrapper'
import * as pdfFonts from 'pdfmake/build/vfs_fonts'
import moment from 'moment'
import _ from 'lodash'

export const ReportePage = () => {
  const [dates, setDates] = useState()
  const [informacionReporte, setInformacionReporte] = useState({
    intencionesContadas: {},
    palabrasPorFecha: [],
    palabrasRepeticion: {},
    total: 0
  })
  const [enviado, setEnviado] = useState(false)
  const [cargando, setCargando] = useState(false)
  const [noData, setNoData] = useState(true)
  const [disabledGenerate, setDisabledGenerate] = useState(false)
  const cref = useRef(null)

  const monthNavigatorTemplate = (e: any) => {
    return <Dropdown value={e.value} options={e.options} onChange={(event) => e.onChange(event.originalEvent, event.value)} style={{ lineHeight: 1 }}/>
  }

  const yearNavigatorTemplate = (e: any) => {
    return <Dropdown value={e.value} options={e.options} onChange={(event) => e.onChange(event.originalEvent, event.value)} className="p-ml-2" style={{ lineHeight: 1 }}/>
  }
  const leftToolbarTemplate = (
    <>
      <label htmlFor="icon"></label>
      <Calendar id="icon" value={dates} onChange={(e) => setDates(e.value)}
                showButtonBar showIcon selectionMode="range" readOnlyInput
                yearRange="2010:2030"
                monthNavigator yearNavigator
                monthNavigatorTemplate={monthNavigatorTemplate}
                yearNavigatorTemplate={yearNavigatorTemplate}
      />
    </>
  )

  const conversionAFechas = () => {
    if (!dates) {
      console.log('error no hay fechas')
      return
    }

    if (dates[0] && dates[1]) {
      return {
        inicio: dates[0],
        fin: dates[1]
      }
    }
  }

  const downloadImage = useCallback(() => {
    console.log(cref)
    const link = document.createElement('a')
    link.download = 'chart.png'
    link.href = cref.current.canvas.toBase64Image()
    link.click()
  }, [])

  const div2PDF = async () => {
    const reporteDos = window.document.getElementById('pdfsample2').getElementsByTagName('canvas')[0]
    const reporteUno = window.document.getElementById('pdfsample1').getElementsByTagName('canvas')[0]

    const canvasUno = await html2canvas(reporteUno)
    const canvasDos = await html2canvas(reporteDos)
    const imgUno = canvasUno.toDataURL('image/png')
    const imgDos = canvasDos.toDataURL('image/png')
    const pdf = new jsPDF('l', 'px')
    console.log(reporteUno)
    console.log(reporteUno.clientHeight, reporteUno.clientWidth)
    pdf.addImage(imgUno, 'png', 0, 50, 330, 330)
    pdf.addImage(imgDos, 'png', 340, 50, 280, 250)
    pdf.save('chart.pdf')
  }

  const descargarPDF = async () => {
    PdfMakeWrapper.setFonts(pdfFonts)
    const pdf = new PdfMakeWrapper()
    const reporteUno = window.document.getElementById('pdfsample1').getElementsByTagName('canvas')[0]
    const reporteDos = window.document.getElementById('pdfsample2').getElementsByTagName('canvas')[0]

    const canvasUno = await html2canvas(reporteUno)
    const canvasDos = await html2canvas(reporteDos)
    const imgUno = canvasUno.toDataURL('image/png')
    const imgDos = canvasDos.toDataURL('image/png')

    const graficoUno = await new Img(imgUno).fit([450, 450]).build()
    const graficoDos = await new Img(imgDos).fit([450, 450]).build()

    pdf.add([
      graficoUno,
      new Txt('').pageBreak('after').end,
      graficoDos,
      new Txt('').pageBreak('after').end,
      new Txt('fecha Emision: ' + moment().format('DD-MM-YYYY')).alignment('right').bold().end,
      new Txt('Reporte de preguntas por fecha').bold().alignment('center').fontSize(20).margin(20).end,
      crearTabla()
    ])
    pdf.create().download('example.pdf')
  }

  const crearTabla = () => {
    const header = [
      new Txt('Fecha').bold().color('#fafafa').fontSize(14).alignment('left').end,
      new Txt('Pregunta').bold().color('#fafafa').fontSize(14).alignment('left').end,
      new Txt('Cantidad').bold().color('#fafafa').fontSize(14).alignment('left').end
    ]
    const arregloPorFecha = informacionReporte.palabrasPorFecha.reduce((acc, { fecha, palabras }) => {
      const keys = Object.keys(palabras)
      keys.map((key: string) => acc.push([fecha, key, palabras[key]]))
      return acc
    }, [])
    return new Table([
      header,
      ...arregloPorFecha
    ])
      .widths(['*', '*', '*'])
      .layout({
        fillColor: (rowIndex) => {
          if (rowIndex === 0) {
            return '#5a58ec'
          }

          if (rowIndex % 2 === 1) {
            return '#ffffff'
          } else {
            return '#D5D5D5'
          }
        }
      }).fontSize(12)
      .alignment('center').end
  }

  const rigthToolbarTemplate = (
    <div className="my-2">
      {
        !noData &&
        <Button label="PDF" icon="pi pi-pdf" className="p-button-danger mr-2" onClick={descargarPDF}/>
      }
      <Button label="Generar" icon="pi pi-sync" disabled={disabledGenerate} className="p-button-success mr-2" onClick={async () => {
        setDisabledGenerate(true)
        setCargando(true)
        const fechas = conversionAFechas()
        const dataset = await obtenerTotalPreguntas(fechas)
        const resultadoVacio = _.isEmpty(dataset?.intencionesContadas) && _.isEmpty(dataset?.palabrasRepeticion) && dataset?.palabrasPorFecha
        setNoData(resultadoVacio)
        setInformacionReporte(dataset)
        setEnviado(true)
        setCargando(false)
        setDisabledGenerate(false)
      }}/>
    </div>
  )

  const MensajeSeleccionPeriodo = (cargando
    ? <div className='grid text-center'>
        <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}/>
      </div>
    : <div className='card col-12'>Seleccione el periodo</div>
  )

  const MensajePeriodoVacio = (cargando
    ? <div className='grid text-center'>
        <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}/>
      </div>
    : <div className="grid " style={{ marginTop: '0.2rem' }}>
        <div className="col-12 md:col-6 lg:col-12">
          <div className="card">
            No existen datos para el periodo seleccionado
          </div>
        </div>
      </div>
  )

  console.log(enviado)
  console.log(noData)
  // @ts-ignore
  const Reportes = (
    <>
      <div className="grid " style={{ marginTop: '0.2rem' }}>
        {
          !_.isEmpty(informacionReporte?.intencionesContadas) &&
          <div className="xs:col-12 sm:col-12 md:col-12 lg:col-6 p-pr-2">
            {informacionReporte &&
              <ChartPorcentajeIntenciones intencionesContadas={informacionReporte.intencionesContadas} ref={cref}/>
            }
          </div>
        }

        {
          !_.isEmpty(informacionReporte?.palabrasRepeticion) &&
          <div className="xs:col-12 sm:col-12 md:col-6 lg-col-6">
            {informacionReporte &&
              <ChartPalabrasMasBuscadas palabrasRepeticion={informacionReporte.palabrasRepeticion}/>
            }
          </div>
        }
      </div>
      <div className="grid" style={{ marginTop: '0.2rem' }}>
        {
          informacionReporte?.palabrasPorFecha.length > 0 &&
          <div className="col-12 md:col-6 lg:col-12">
            {informacionReporte &&
              <ChartPalabrasPorFecha palabrasPorFecha={informacionReporte.palabrasPorFecha}/>
            }
          </div>
        }
      </div>
    </>)

  return (
    <>
      <div className="grid">
        <div className="col-12">
          <Toolbar className="card mb-4" left={leftToolbarTemplate} right={rigthToolbarTemplate}></Toolbar>
        </div>
      </div>
      {
        (enviado && noData) && MensajePeriodoVacio
      }
      { !enviado ? MensajeSeleccionPeriodo : Reportes }

    </>
  )
}
