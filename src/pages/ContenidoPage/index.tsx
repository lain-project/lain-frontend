import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { Button } from 'primereact/button'
import { useNavigation } from '@/hooks/useNavigation.hook'
import { useEffect, useRef, useState } from 'react'
import { useStoreContenido } from '@/zustand/store/contenido.store'
import { ContenidoTable } from '@/pages/ContenidoPage/components/ContenidoTable'
import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { ContenidoDialog } from '@/pages/ContenidoPage/components/ContenidoDialog'
import { useNavigate } from 'react-router-dom'
import { ConfirmDialog } from 'primereact/confirmdialog'
import { Spiner } from '@/components/Spiner'
import { eliminarContenido } from '@/api/contenido.service'

export const ContenidoPage = () => {
  const toast = useRef<Toast>(null)
  const { volver } = useNavigation()
  const [mostrarDialog, setMostrarDialog] = useState(false)
  const [mostrarDialogDelete, setMostrarDialogDelete] = useState(false)
  const { unidadSeleccionada } = useStoreUnidad()
  const [loading, setLoading] = useState(false)
  const { contenidosMateria, getContenidos, contenidoSeleccionado, setContenidoSeleccionado } = useStoreContenido()
  const navigate = useNavigate()

  useEffect(() => {
    getContenidos(unidadSeleccionada.id)
  }, [])

  const openNew = async () => {
    setMostrarDialog(true)
  }

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Volver" icon="pi pi-arrow-left" className="p-button-danger mr-2" onClick={volver}/>
        </div>
      </>
    )
  }

  const rigthToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Nuevo" icon="pi pi-plus" className="p-button-success mr-2" onClick={() => navigate('agregar')}/>
        </div>
      </>
    )
  }

  const eliminar = async () => {
    const idContenido = contenidoSeleccionado.id
    setLoading(true)
    await eliminarContenido(idContenido)
    await getContenidos(unidadSeleccionada.id)
    setContenidoSeleccionado(null)
    setMostrarDialogDelete(false)
    setLoading(false)
  }

  const Footer = (
    (!loading
      ? <div>
        <Button label="Cancelar"
                icon="pi pi-times"
                className="p-button-danger"
                onClick={() => setMostrarDialogDelete(false)}/>
        <Button label={'Guardar'}
                icon="pi pi-save"
                className="p-button-success"
                onClick={async () => await eliminar()}
        />
      </div>
      : <div></div>)
  )

  const loadingDialogProps = {
    message: (<Spiner/>),
    header: 'Guardando...',
    closable: false

  }
  const messageConfirm = {
    message: '¿Desea eliminar el contenido?',
    header: 'Confirmar eliminación',
    closable: false
  }

  return (
    <>
      {contenidosMateria && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" left={leftToolbarTemplate} right={rigthToolbarTemplate}></Toolbar>
               <ConfirmDialog
                visible={mostrarDialogDelete}
                footer={Footer}
                style={{ width: '30rem' }}
                {...(loading ? loadingDialogProps : messageConfirm)}
               />

            </div>
            <ContenidoTable
              openDeleteDialog={() => setMostrarDialogDelete(true)}
            />
          </div>
        </div>)}
    </>
  )
}
