import { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import { Column } from 'primereact/column'
import { DataTable } from 'primereact/datatable'
import { Button } from 'primereact/button'
import { Spiner } from '@/components/Spiner'
import { Table } from '@/components/Table/Table'
import { useStoreContenido } from '@/zustand/store/contenido.store'
import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { Contenido } from '@/model/contenido.model'

export const ContenidoTable = ({ openDeleteDialog }: any) => {
  const { contenidosMateria, loading, setToUpdate, setContenidoSeleccionado} = useStoreContenido()
  const { unidadSeleccionada } = useStoreUnidad()
  const tableRef = useRef<DataTable>(null)
  const navigate = useNavigate()

  const actionBodyTemplate = (contenido: Contenido) => (
    <>
      <Button
        icon="pi pi-pencil"
        className="p-button-rounded p-button-success mr-2"
        tooltip={'Editar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setToUpdate(contenido)
          navigate('agregar')
        }}
      />
      <Button
        icon="pi pi-trash"
        className="p-button-rounded p-button-danger mr-2"
        tooltip={'Eliminar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setContenidoSeleccionado(contenido)
          openDeleteDialog(true)
        }}
      />
    </>
  )

  const templateMinimo = (contenido: Contenido) => (<span>{contenido.esMinimo ? 'Si' : 'No'}</span>)

  return (
    <>
      { loading && <Spiner/> }
      { !loading && (
        <Table tableRef={tableRef}
               data={contenidosMateria}
               title={`Contenido de la unidad ${unidadSeleccionada.nombre}`}
               emptyMessage={'No se asigno ningun contenido a la unidad'}
               loading={loading}>
          <Column field="nombre" header="Nombre" sortable></Column>
          <Column field="contenidoMinimo" header="Contenido Minimo" sortable
          body={templateMinimo}></Column>
          <Column body={actionBodyTemplate}></Column>
        </Table>
      )}
    </>
  )
}
