import { FunctionComponent, useEffect, useRef, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import { Unidad } from '@/model/unidad.model'
import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { Button } from 'primereact/button'
import { Toast } from 'primereact/toast'
import { Dialog } from 'primereact/dialog'
import { InputText } from 'primereact/inputtext'
import { Editor } from 'primereact/editor'
import { useStoreContenido } from '@/zustand/store/contenido.store'
import { actualizarContenido, asignarContenido } from '@/api/contenido.service'
import { Contenido } from '@/model/contenido.model'

export const ContenidoDialog: FunctionComponent<any> = ({ mostrarDialog, cerrarDialog }: any) => {
  const toast = useRef<any>(null)
  const { register, handleSubmit, reset, formState: { errors }, control, setValue } = useForm<Unidad>()
  const { unidadSeleccionada } = useStoreUnidad()
  const { contenidoAActualizar, setToUpdate, getContenidos } = useStoreContenido()
  const [isUpdated, setIsUpdated] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (contenidoAActualizar) {
      reset({ ...contenidoAActualizar })
      setIsUpdated(true)
    } else {
      reset({ ...register })
      setIsUpdated(false)
    }
  }, [mostrarDialog, cerrarDialog])

  const guardarContenido = async (contenido: Contenido) => {
    try {
      setLoading(true)
      if (isUpdated) {
        await actualizarContenido(contenido)
      } else {
        await asignarContenido(contenido)
      }
      setToUpdate(null)
      reset({ ...register })
      getContenidos(unidadSeleccionada.id)
      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Contenido creado exitosamente', life: 3000 })
      setLoading(false)
      cerrarDialog()
    } catch (error: any) {
      setLoading(false)
      const message = (error as Error).message
      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: message, life: 3000 })
    }
  }

  const dialogFooter = (
      <>
        <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={cerrarDialog}/>
        <Button label={ loading ? 'Guardar' : 'Actualizar'}
                icon="pi pi-check"
                className="p-button-text"
                onClick={handleSubmit(guardarContenido)}/>
      </>
  )

  const dialogProps = {
    header: (isUpdated ? 'Actualizar ' : 'Crear ') + 'contenido',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: dialogFooter
  }

  const loadingDialogProps = {
    header: 'Guardando...'
  }

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  return (
      <>
        <Toast ref={toast} />
        <Dialog visible={mostrarDialog}
                style={{ width: '450px' }}
                closable={false}
                className='p-fluid'
                onHide={cerrarDialog}
                {...(loading ? loadingDialogProps : dialogProps)}>
          {loading && <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}></i>}
          <div className={loading ? 'blur' : ''}>
            <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="Nombre" {...register('nombre', { required: 'El nombre es obligatorio' })}/>
            </span>
              {getFormErrorMessage('nombre')}
            </div>
            <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="Numero"
                         {...register('numero', { required: 'El numero es obligatorio' })}/>
            </span>
              {getFormErrorMessage('numero')}
            </div>
            <div className="field">
            <span className="p-input-icon-left">
                <Controller
                  name="objetivos"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <Editor
                      style={{ height: '13rem' }}
                      value={value}
                      readOnly={loading}
                      onTextChange={(e) => onChange(e.htmlValue || '') }/>
                  )}
                />
            </span>
              {getFormErrorMessage('objetivos')}
            </div>
          </div>
        </Dialog>
      </>
  )
}
