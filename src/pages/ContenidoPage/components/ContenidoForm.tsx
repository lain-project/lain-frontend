import { Editor } from 'primereact/editor'
import { Divider } from 'primereact/divider'
import { RadioButton } from 'primereact/radiobutton'
import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { Button } from 'primereact/button'
import { useStoreContenido } from '@/zustand/store/contenido.store'
import { useNavigate } from 'react-router-dom'
import { InputTextarea } from 'primereact/inputtextarea'
import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { actualizarContenido, asignarContenido } from '@/api/contenido.service'
import { Controller, useForm } from 'react-hook-form'
import { Contenido } from '@/model/contenido.model'

export const ContenidoForm = () => {
  const toast = useRef<Toast>(null)
  const [minimo, setMinimo] = useState(true)
  const [actualizar, setActualizar] = useState(false)
  const [loading, setLoading] = useState(false)
  const { unidadSeleccionada } = useStoreUnidad()
  const { contenidoAActualizar, setToUpdate } = useStoreContenido()
  const navigate = useNavigate()
  const { register, handleSubmit, reset, formState: { errors }, control } = useForm<Contenido>()

  useEffect(() => {
    if (contenidoAActualizar) {
      reset({ ...contenidoAActualizar })
      setMinimo(contenidoAActualizar?.esMinimo)
      setActualizar(true)
    } else {
      reset({ ...register })
      setActualizar(false)
    }
  }, [])

  const actualizacion = async (content: Contenido) => {
    const contenido = {
      id: contenidoAActualizar.id,
      nombre: content.nombre,
      descripcion: content.descripcion,
      esMinimo: minimo,
      unidadId: unidadSeleccionada.id
    }
    setLoading(true)
    await actualizarContenido(contenido)
    setLoading(false)
    setToUpdate(null)
    navigate(-1)
  }
  const guardar = async (content: Contenido) => {
    const contenido = {
      nombre: content.nombre,
      descripcion: content.descripcion,
      esMinimo: minimo,
      unidadId: unidadSeleccionada.id
    }

    setLoading(true)
    await asignarContenido(contenido)
    setLoading(false)
    navigate(-1)
  }

  const rightToolbarTemplate = () => {
    const func = actualizar ? actualizacion : guardar
    return (
      <div className="my-2">
        <Button label={actualizar ? 'Actualizar' : 'Guardar'}
                icon="pi pi-plus"
                className="p-button-success mr-2"
                disabled={loading}
                onClick={handleSubmit(func, (e) => console.log(e))}/>
      </div>
    )
  }

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button
            label="Volver"
            icon="pi pi-arrow-left"
            className="p-button-danger mr-2"
            onClick={() => {
              setToUpdate(null)
              navigate(-1)
            }
            }/>
        </div>
      </>
    )
  }

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  return (
    <>
      <div className="grid crud-demo">
        <div className="col-12">
          <div className="card">
            <Toast ref={toast}/>
            <Toolbar className="mb-4" right={() => rightToolbarTemplate()} left={leftToolbarTemplate}></Toolbar>
          </div>
        </div>
        <div className="col-12">
          <div className="p-datatable">
            <div className="p-datatable-header">
              {loading
                ? <div className={'flex flex-column align-content-center'}>
                  <h4 className={'align-self-center'}>{!actualizar ? 'Guardando...' : 'Actualizando...'}</h4>
                  <div className={'align-self-center'}>
                    <i className="pi pi-spin pi-spinner" style={{ fontSize: '2em' }}/>
                  </div>
                </div>
                : <div>
                  <div>
                    <h4>Nombre</h4>
                    <Divider/>
                    <div>
                      <Controller
                        name="nombre"
                        control={control}
                        rules={{ required: 'El nombre es requerido' }}
                        render={({ field: { onChange, value } }) => (
                          <InputTextarea
                            className='w-100'
                            disabled={loading}
                            value={value}
                            onChange={(e) => onChange(e.target.value || '')}
                          />
                        )} />
                    </div>
                    {getFormErrorMessage('nombre')}
                  </div>
                  <div className={'pt-4'}>
                    <h4>Descripcion</h4>
                    <Divider/>
                    <Controller
                      name="descripcion"
                      control={control}
                      rules={{ required: 'La descripcion es requerida' }}
                      render={({ field: { onChange, value } }) => (
                        <Editor
                          style={{ height: '13rem' }}
                          value={value}
                          readOnly={loading}
                          onTextChange={e => onChange(e.htmlValue)}/>
                      )}
                    />
                    {getFormErrorMessage('descripcion')}
                  </div>
                  <div className={'pt-4'}>
                    <h4>Contenido Minimo</h4>
                    <Divider/>
                    <div className={'pt-3'}>
                      <RadioButton inputId="esMinimo" name="minimo" value={true} onChange={(e) => setMinimo(e.value)} checked={minimo}/>
                      <label className={'pl-2'} htmlFor="esMinimo">Es contenido minimo</label>
                    </div>
                    <div className={'pt-3'}>
                      <RadioButton inputId="noMinimo" name="minimo" value={false} onChange={(e) => setMinimo(e.value)} checked={!minimo}/>
                      <label htmlFor="noMinimo" className={'pl-2'}>No es contenido minimo</label>
                    </div>
                  </div>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
