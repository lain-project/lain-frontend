import { FunctionComponent, useEffect, useRef, useState } from 'react'
import { useStoreMaterias } from '@/zustand/store'
import { Toast } from 'primereact/toast'
import { Button } from 'primereact/button'
import { Controller, useForm } from 'react-hook-form'
import { Dialog } from 'primereact/dialog'
import { Correlativa, CorrelativaCursar, CorrelativaFinal } from '@/model/correlativa.model'
import { useStoreCorrelativa } from '@/zustand/store/correlativa.store'
import { asignarCorrelativa } from '@/api/correlativa.service'
import { AutoComplete } from 'primereact/autocomplete'
import { RadioButton } from 'primereact/radiobutton'

export const CorrelativaDialog: FunctionComponent<any> = ({ mostrarDialog, cerrarDialog }: any) => {
  const toast = useRef<any>(null)
  const { register, handleSubmit, reset, formState: { errors }, control, setValue } = useForm<Correlativa>()
  const { materiaSeleccionada } = useStoreMaterias()
  const { setToUpdate, getCorrelativas, materiasCorrelativas } = useStoreCorrelativa()
  const [loading, setLoading] = useState(false)
  const [selectedMateria, setSelectedMateria] = useState([])
  const [filteredMaterias, setFilteredMaterias] = useState([])

  useEffect(() => {
    reset({ ...register })
  }, [mostrarDialog, cerrarDialog])

  const guardarCorrelativa = async (correlativa: Correlativa) => {
    const correlativas = selectedMateria.map(materia => ({
      correlativaId: materia?.id,
      materiaId: materiaSeleccionada.id,
      ...correlativa
    }))
    try {
      setLoading(true)
      const promises = correlativas.map(correlative => asignarCorrelativa(correlative))
      await Promise.all(promises)
      setToUpdate(null)
      reset({ ...register })
      await getCorrelativas(materiaSeleccionada.id)
      setSelectedMateria([])
      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Correlativa creada exitosamente', life: 3000 })
      setLoading(false)
      cerrarDialog()
    } catch (error: any) {
      setSelectedMateria([])
      setLoading(false)
      const message = (error as Error).message
      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: message, life: 3000 })
    }
  }

  const dialogFooter = (
    <>
      <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={cerrarDialog}/>
      <Button label={ 'Guardar'}
              icon="pi pi-check"
              className="p-button-text"
              onClick={handleSubmit(guardarCorrelativa)}/>
    </>
  )

  const dialogProps = {
    header: 'Crear correlativa',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: dialogFooter
  }

  const loadingDialogProps = {
    header: 'Guardando...'
  }

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  const searchMateria = (event: any) => {
    let _filteredMaterias = []
    if (!event.query.trim().length) {
      _filteredMaterias = [...materiasCorrelativas]
    } else {
      _filteredMaterias = materiasCorrelativas.filter((materia) => {
        return materia?.nombre.toLowerCase().startsWith(event.query.toLowerCase())
      })
    }

    setFilteredMaterias(_filteredMaterias)
  }
  return (
    <>
      <Toast ref={toast} />
      <Dialog visible={mostrarDialog}
              style={{ width: '450px' }}
              closable={false}
              className='p-fluid'
              onHide={cerrarDialog}
              {...(loading ? loadingDialogProps : dialogProps)}>
        {loading && <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}></i>}
        <div className={loading ? 'blur' : ''}>
          <div className="field">
            <label className="mb-3">Materia <br />{getFormErrorMessage('correlativaId')}</label>
            <AutoComplete value={selectedMateria}
                          suggestions={filteredMaterias}
                          completeMethod={searchMateria}
                          field="nombre"
                          multiple
                          onChange={(e) => setSelectedMateria(e.value)}
                          aria-label="Materias"
                          dropdownAriaLabel="Seleccione las materias" />
          </div>
          <div className="field">
            <label className="mb-3">Necesaria para cursar <br />{getFormErrorMessage('necesariaParaCursar')}</label>
            <Controller
              name="necesariaParaCursar"
              control={control}
              rules={{ required: 'El campo es requerido' }}
              render={({ field: { onChange, value } }) => (
                <div className="formgrid grid">
                  <div className="field-radiobutton col-12">
                    <RadioButton inputId="category1" value={0} onChange={onChange} checked={value === CorrelativaCursar.NO_APLICA} />
                    <label htmlFor="category1">No aplica</label>
                  </div>
                  <div className="field-radiobutton col-12">
                    <RadioButton inputId="category2" value={1} onChange={onChange} checked={value === CorrelativaCursar.REGULARIZADA} />
                    <label htmlFor="category2">Regularizada</label>
                  </div>
                  <div className="field-radiobutton col-12">
                    <RadioButton inputId="category3" value={2} onChange={onChange} checked={value === CorrelativaCursar.APROBADA} />
                    <label htmlFor="category2">Aprobada</label>
                  </div>
                </div>)
              } />
          </div>
          <div className="field">
            <label className="mb-3">Necesaria para rendir final <br />{getFormErrorMessage('necesariaParaRendirFinal')}</label>
            <Controller
              name="necesariaParaRendirFinal"
              control={control}
              rules={{ required: 'El campo es requerido' }}
              render={({ field: { onChange, value } }) => (
                <div className="formgrid grid">
                  <div className="field-radiobutton col-12">
                    <RadioButton inputId="category4" value={0} onChange={onChange} checked={value === CorrelativaFinal.NO_APLICA} />
                    <label htmlFor="category1">No aplica</label>
                  </div>
                  <div className="field-radiobutton col-12">
                    <RadioButton inputId="category5" value={1} onChange={onChange} checked={value === CorrelativaFinal.APROBADA} />
                    <label htmlFor="category2">Aprobada</label>
                  </div>
                </div>)
              } />
          </div>
        </div>
      </Dialog>
    </>
  )
}
