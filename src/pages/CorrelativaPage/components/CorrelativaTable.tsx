import { useStoreMaterias } from '@/zustand/store'
import { useRef } from 'react'
import { DataTable } from 'primereact/datatable'
import { Button } from 'primereact/button'
import { Spiner } from '@/components/Spiner'
import { Table } from '@/components/Table/Table'
import { Column } from 'primereact/column'
import { useStoreCorrelativa } from '@/zustand/store/correlativa.store'
import { Correlativa } from '@/model/correlativa.model'

export const CorrelativaTable = ({ openDialog, openDeleteCorrelativa }: any) => {
  const { correlativasMateria, loading, setToUpdate, setCorrelativaSeleccionada } = useStoreCorrelativa()
  const { materiaSeleccionada } = useStoreMaterias()

  const tableRef = useRef<DataTable>(null)

  const actionBodyTemplate = (correlativa: Correlativa) => (
    <>
      <Button
        icon="pi pi-trash"
        className="p-button-rounded p-button-danger mr-2"
        tooltip={'Eliminar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setCorrelativaSeleccionada(correlativa)
          openDeleteCorrelativa()
        }}
      />
    </>
  )

  const requisitoParaCursar = (correlativa: Correlativa) => (
    <span>
      {correlativa.necesariaParaCursar === 0
        ? '-'
        : correlativa.necesariaParaCursar === 1
          ? 'Regularizada'
          : 'Aprobada'
      }
    </span>
  )

  const requisitoParaFinal = (correlativa: Correlativa) => (
    <span>
      {correlativa.necesariaParaRendirFinal === 0
        ? '-'
        : 'Aprobada'
      }
    </span>
  )
  return (
    <>
      { loading && <Spiner/> }
      { !loading && (
        <Table tableRef={tableRef}
               data={correlativasMateria}
               title={`Correlativas de la materia ${materiaSeleccionada.nombre}`}
               emptyMessage={'No se agrego ninguna correlativa a la materia'}
               loading={loading}>
          <Column field="correlativa.nombre" header="Nombre de la materia" sortable></Column>
          <Column field="necesariaParaCursar" header="Requisito para cursar" body={requisitoParaCursar} sortable></Column>
          <Column field="necesariaParaRendirFinal" header="Requisito para final" body={requisitoParaFinal} sortable></Column>
          <Column body={actionBodyTemplate}></Column>
        </Table>
      )}
    </>
  )
}
