import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { useStoreMaterias } from '@/zustand/store'
import { Button } from 'primereact/button'
import { Toolbar } from 'primereact/toolbar'
import { useNavigation } from '@/hooks/useNavigation.hook'
import { ConfirmDialog } from 'primereact/confirmdialog'
import { useStoreCorrelativa } from '@/zustand/store/correlativa.store'
import { eliminarCorrelativa } from '@/api/correlativa.service'
import { CorrelativaTable } from '@/pages/CorrelativaPage/components/CorrelativaTable'
import { CorrelativaDialog } from '@/pages/CorrelativaPage/components/CorrelativaDialog'

export const CorrelativaPage = () => {
  const toast = useRef<Toast>(null)
  const { getCorrelativas, correlativasMateria, correlativaSeleccionada, getMateriasCorrelativas } = useStoreCorrelativa()
  const { materiaSeleccionada } = useStoreMaterias()
  const [mostrarDialog, setMostrarDialog] = useState(false)
  const [mostrarDeleteDialog, setMostrarDeleteDialog] = useState(false)
  const { volver } = useNavigation()
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    getCorrelativas(materiaSeleccionada.id)
  }, [])

  const openNew = async () => {
    setMostrarDialog(true)
  }

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Volver" icon="pi pi-arrow-left" className="p-button-danger mr-2" onClick={volver}/>
        </div>
      </>
    )
  }

  const rigthToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Nuevo" icon="pi pi-plus" className="p-button-success mr-2" onClick={async () => {
            await getMateriasCorrelativas(materiaSeleccionada.id)
            await openNew()
          }}/>
        </div>
      </>
    )
  }

  const Footer = (
    (!loading
      ? <div>
        <Button label="Cancelar"
                icon="pi pi-times"
                className="p-button-danger"
                onClick={() => setMostrarDeleteDialog(false)}/>
        <Button label={'Guardar'}
                icon="pi pi-save"
                className="p-button-success"
                onClick={async () => {
                  setLoading(true)
                  await eliminarCorrelativa(correlativaSeleccionada?.id)
                  await getCorrelativas(materiaSeleccionada.id)
                  setLoading(false)
                  setMostrarDeleteDialog(false)
                }}
        />
      </div>
      : <div></div>)
  )
  const messageConfirm = {
    message: '¿Desea eliminar la materia correlativa?',
    header: 'Confirmar eliminación',
    closable: false
  }
  const loadingDialogProps = {
    header: 'Guardando...'
  }

  return (
    <>
      {correlativasMateria && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" left={leftToolbarTemplate} right={rigthToolbarTemplate}></Toolbar>
              <ConfirmDialog
                visible={mostrarDeleteDialog}
                footer={Footer}
                style={{ width: '30rem' }}
                {...(loading ? loadingDialogProps : messageConfirm)}
              />
              <CorrelativaDialog
                mostrarDialog={mostrarDialog}
                cerrarDialog={() => setMostrarDialog(false)} />
            </div>
            <CorrelativaTable
              openDialog={() => setMostrarDialog(true)}
              openDeleteCorrelativa={() => setMostrarDeleteDialog(true)}
            />
          </div>
        </div>)}
    </>
  )
}
