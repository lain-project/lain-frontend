import { useParams } from 'react-router-dom'
import { useEffect, useRef, useState } from 'react'
import { StudyPlanModel } from '@/model/study-plan.model'
import { useStoreAnios, useStoreMaterias, useStoreStudyPlan } from '@/zustand/store'
import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { MateriaTable } from '@/pages/MateriaPage/components/MateriaTable'
import { MateriaDialog } from '@/pages/MateriaPage/components/MateriaDialog'
import { useRol } from '@/hooks/useRol.hook'
import { ButtonNew } from '@/components/ButtonNew'
import { ButtonBack } from '@/components/ButtonBack'

export const MateriaPage = () => {
  const toast = useRef<Toast>(null)
  const { id } = useParams()
  const { isAdmin } = useRol()
  const [planDeEstudio, setPlanDeEstudio] = useState<StudyPlanModel | null>()
  const [mostrarDialog, setMostrarDialog] = useState(false)
  const { getStudyPlans, studysPlans } = useStoreStudyPlan()
  const { getMaterias } = useStoreMaterias()
  const { obtenerAnios } = useStoreAnios()

  const getStudyPlan = async () => {
    await getStudyPlans()
    return studysPlans.find(studyPlan => studyPlan.id === Number(id))
  }
  useEffect(() => {
    getStudyPlan().then(async plan => {
      await getMaterias(id)
      await obtenerAnios(id)
      setPlanDeEstudio(plan)
    })
  }, [])

  const openNew = () => {
    setMostrarDialog(true)
  }

  const nuevo = (isAdmin() && <ButtonNew onClick={openNew} />)
  const volver = (<ButtonBack onClick={() => setPlanDeEstudio(null)}/>)

  return (
    <>
      {planDeEstudio && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" left={volver} right={nuevo}></Toolbar>
              <MateriaDialog mostrarDialog={mostrarDialog} onClose={() => setMostrarDialog(false)} planEstudio={id}/>
            </div>
            <MateriaTable openDialog={openNew} planDeEstudio={planDeEstudio}/>
          </div>
        </div>)
      }
    </>
  )
}
