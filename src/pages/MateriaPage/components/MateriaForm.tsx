import { Controller } from 'react-hook-form'
import { Materia, Regimen } from '@/model/materia'
import { InputText } from 'primereact/inputtext'
import { RadioButton } from 'primereact/radiobutton'
import { Dropdown } from 'primereact/dropdown'
import { UseFormHandleSubmit } from 'react-hook-form/dist/types/form'

interface MateriaFormProps {
  materia?: Materia
  anios?: any[]
  handleSubmit?: UseFormHandleSubmit<Materia>
  errors?: any
  register?: any
  control?: any
  className?: string
}

export const MateriaForm = ({ anios, errors, register, control, className }: MateriaFormProps) => {
  const formErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }
  const validacionCodigo = { ...register('codigo', { required: 'El código es obligatorio' }) }
  const validacionNombre = { ...register('nombre', { required: 'El nombre es obligatorio' }) }
  const validacionHorasSemanales = { ...register('horasSemanales', { pattern: { value: /^\d+$/, message: 'El valor ingresado no es valido' } }) }
  const validacionHorasTotales = { ...register('horasTotales', { pattern: { value: /^\d+$/, message: 'El valor ingresado no es valido' } }) }
  return (
    <div className={className}>
      <div className="field">
        <span className="p-input-icon-left">
          <InputText type="text" autoComplete='false' placeholder="Código" {...validacionCodigo}/>
        </span>
        {formErrorMessage('codigo')}
      </div>
      <div className="field">
        <span className="p-input-icon-left">
          <InputText type="text" autoComplete='false' placeholder="Nombre" {...validacionNombre}/>
        </span>
        {formErrorMessage('nombre')}
      </div>
      <div className="field">
        <span className="p-input-icon-left">
          <InputText type="text" autoComplete='false' placeholder="Horas Semanales" {...validacionHorasSemanales} />
        </span>
        {formErrorMessage('horasSemanales')}
      </div>
      <div className="field">
        <span className="p-input-icon-left">
          <InputText type="text" autoComplete='false' placeholder="Horas Totales" {...validacionHorasTotales}/>
        </span>
        {formErrorMessage('horasTotales')}
      </div>
      <div className="field">
        <label className="mb-3">Regimen<br />{formErrorMessage('regimen')}</label>
        <Controller
          name="regimen"
          control={control}
          rules={{ required: 'El regimen es requerido.' }}
          render={({ field: { onChange, value } }) => (
            <div className="formgrid grid">
              <div className="field-radiobutton col-12">
                <RadioButton inputId="regimen-anual" value={'ANUAL'} onChange={onChange} checked={value?.toUpperCase() === Regimen.ANUAL} />
                <label htmlFor="regimen-anual">Anual</label>
              </div>
              <div className="field-radiobutton col-12">
                <RadioButton inputId="regimen-cuatrimestral" value={'CUATRIMESTRAL'} onChange={onChange} checked={value?.toUpperCase() === Regimen.CUATRIMESTRAL} />
                <label htmlFor="regimen-cuatrimestral">Cuatrimestral</label>
              </div>
            </div>)
          }
        />
      </div>
      <div className="field">
        <label className="mb-3">Año<br />{formErrorMessage('anioPlanEstudio')}</label>
        <Controller
          name="anioPlanEstudio"
          control={control}
          rules={{ required: 'El año es requerido.' }}
          render={({ field }) => (
            <Dropdown
              value={field.value}
              onChange={(e) => field.onChange(e.value)}
              options={anios}
              optionLabel="nombre"
              placeholder="Select"
            />
          )} />
      </div>
    </div>
  )
}
