import { FunctionComponent, useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { Dialog } from 'primereact/dialog'
import { Button } from 'primereact/button'
import { MateriaForm } from '@/pages/MateriaPage/components/MateriaForm'
import { useForm } from 'react-hook-form'
import { Materia } from '@/model/materia'
import { useStoreAnios, useStoreMaterias } from '@/zustand/store'
import { actualizarMateria, guardarMateria } from '@/api/materia.services'

export const MateriaDialog: FunctionComponent<any> = ({ mostrarDialog, onClose, planEstudio }: any) => {
  const { anios } = useStoreAnios()
  const { getMaterias, materiaActualizar, setMateriaActualizar } = useStoreMaterias()
  const [loading, setLoading] = useState(false)
  const toast = useRef<Toast>(null)
  const { register, handleSubmit, reset, formState: { errors }, control, setValue } = useForm<Materia>()

  useEffect(() => {
    if (materiaActualizar) {
      setValue('id', materiaActualizar.id)
      setValue('codigo', materiaActualizar.codigo)
      setValue('anio', materiaActualizar.anio)
      setValue('nombre', materiaActualizar.nombre)
      setValue('regimen', materiaActualizar.regimen)
      setValue('anioPlanEstudio', materiaActualizar.anio)
      setValue('horasSemanales', materiaActualizar.horasSemanales)
      setValue('horasTotales', materiaActualizar.horasTotales)
    } else {
      reset({ ...register, anioPlanEstudio: null, regimen: null })
    }
  }, [materiaActualizar])

  const ocultarDialog = async () => {
    reset({ ...register })
    setMateriaActualizar(null)
    onClose()
  }

  const guardar = async (data: any) => {
    setLoading(true)
    const { anioPlanEstudio } = data
    const materiaSub = {
      ...data,
      regimen: data.regimen.toUpperCase(),
      anioPlanEstudio: anioPlanEstudio?.id,
    }
    console.log(materiaSub)
    await guardarMateria(materiaSub)
    setLoading(false)
    await getMaterias(planEstudio)
    await ocultarDialog()
  }

  const actualizar = async (data: any) => {
    setLoading(true)
    const { anioPlanEstudio } = data
    const materiaSub = {
      id: materiaActualizar.id,
      ...data,
      regimen: data.regimen.toUpperCase(),
      anioPlanEstudio: anioPlanEstudio.id

    }
    await actualizarMateria(materiaSub, materiaActualizar.id)
    await getMaterias(planEstudio)
    setLoading(false)
    await ocultarDialog()
  }

  const dialogFooter = (
    <>
      <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={ocultarDialog}/>
      <Button label={materiaActualizar ? 'Actualizar' : 'Guardar'}
              icon="pi pi-check"
              className="p-button-text"
              onClick={ (materiaActualizar ? handleSubmit(actualizar) : handleSubmit(guardar))}/>
    </>
  )

  const dialogProperties = {
    header: (materiaActualizar ? 'Modificar' : 'Crear') + ' materia',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: dialogFooter

  }
  const loadingDialogProps = {
    header: 'Guardando...'
  }

  return (
    <>
      <Toast ref={toast}/>
      <Dialog visible={mostrarDialog}
              style={{ width: '40rem' }}
              closable={false}
              className='p-fluid'
              onHide={() => ocultarDialog()}
              {...(loading ? loadingDialogProps : dialogProperties)}
      >
        {loading && <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}/>}
        <MateriaForm
          className={loading ? 'blur' : ''}
          anios={anios}
          handleSubmit={handleSubmit}
          errors={errors}
          register={register}
          control={control}
        />
      </Dialog>
    </>
  )
}
