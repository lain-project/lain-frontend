import { DataTable } from 'primereact/datatable'
import { Button } from 'primereact/button'
import { Column } from 'primereact/column'
import { useEffect, useRef, useState } from 'react'
import { Table } from '@/components/Table/Table'
import { useStoreAnios, useStoreMaterias } from '@/zustand/store'
import { Materia } from '@/model/materia'
import { Anio } from '@/model/anio.model'
import { useNavigate } from 'react-router-dom'
import { useRol } from '@/hooks/useRol.hook'

export const MateriaTable = ({ openDialog, planDeEstudio }: any) => {
  const navigate = useNavigate()
  const [materiasRender, setMateriasRender] = useState<Materia[]>([])
  const { isAdmin } = useRol()
  const { materias, loading, setMateriaActualizar, setMateriaSeleccionada } = useStoreMaterias()
  const { anios, loading: loadingAnios } = useStoreAnios()

  useEffect(() => {
    const mat = materias.map((materia:Materia) => ({ ...materia, anio: anios.find((anio:Anio) => anio.id === materia.anio) }))
    setMateriasRender(mat)
  }, [materias])
  const tableRef = useRef<DataTable>(null)

  const navegacionADocentes = async (materia: Materia) => {
    await setMateriaSeleccionada(materia)
    navigate(`/materia/${materia.id}/docentes`)
  }

  const navegacionAPropuesta = async (materia: Materia) => {
    await setMateriaSeleccionada(materia)
    navigate(`/materia/${materia.id}/propuesta`)
  }

  const navegacionAUnidades = async (materia: any) => {
    await setMateriaSeleccionada(materia)
    navigate(`/materia/${materia.id}/unidades`)
  }

  const navegacionABibliografia = async (materia: Materia) => {
    await setMateriaSeleccionada(materia)
    navigate(`/materia/${materia.id}/bibliografia`)
  }

  const navegacionACorrelativa = async (materia: Materia) => {
    await setMateriaSeleccionada(materia)
    navigate(`/materia/${materia.id}/correlativa`)
  }

  const actionBodyTemplate = (materia: any) => {
    return (
      <div className="actions flex  justify-content-center gap-4">
        {isAdmin() &&
          <Button icon="pi pi-pencil"
                  className="p-button-rounded p-button-success mr-2"
                  tooltip={'Editar'}
                  tooltipOptions={{ position: 'top' }} onClick={async () => {
                    await setMateriaActualizar(materia)
                    openDialog()
                  }}/>
        }
        <Button icon="pi pi-info-circle"
                className="p-button-rounded p-button-new mr-2"
                tooltipOptions={{ position: 'top' }}
                tooltip={'Propuesta'}
                onClick={async () => await navegacionAPropuesta(materia)}/>
        <Button icon="pi pi-users"
                className="p-button-rounded p-button-help mr-2"
                tooltipOptions={{ position: 'top' }}
                tooltip={'Docentes Asignados'}
                onClick={async () => await navegacionADocentes(materia)}/>
        <Button icon="pi pi-list"
                className="p-button-rounded p-button-danger mr-2"
                tooltipOptions={{ position: 'top' }}
                tooltip={'Unidades'}
                onClick={async () => await navegacionAUnidades(materia)}/>
        <Button icon="pi pi-book"
                className="p-button-rounded p-button-warning mr-2"
                tooltipOptions={{ position: 'top' }}
                tooltip={'Bibliografia'}
                onClick={async () => await navegacionABibliografia(materia)}/>
        <Button icon="pi pi-arrows-h"
                className="p-button-rounded p-button-secondary mr-2"
                tooltipOptions={{ position: 'top' }}
                tooltip={'Correlativas'}
                onClick={async () => await navegacionACorrelativa(materia)}/>

      </div>
    )
  }

  return (
    <>
      { (loading && loadingAnios) && <div>Loading...</div> }
      { (!loading && !loadingAnios) && (
        <Table tableRef={tableRef} data={materiasRender} title={`Materias de ${planDeEstudio.name} - ${planDeEstudio.code}`} loading={loading}>
          <Column field="anio.numero" header="Año" sortable></Column>
          <Column field="codigo" header="Código" sortable></Column>
          <Column field="nombre" header="Nombre" sortable></Column>
          <Column field="regimen" header="Regimen" sortable></Column>
          <Column body={actionBodyTemplate}></Column>
        </Table>)}
    </>
  )
}
