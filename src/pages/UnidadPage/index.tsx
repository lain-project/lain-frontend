import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { useStoreMaterias } from '@/zustand/store'
import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { UnidadTable } from './components/UnidadTable'
import { Button } from 'primereact/button'
import { Toolbar } from 'primereact/toolbar'
import { useNavigation } from '@/hooks/useNavigation.hook'
import { UnidadDialog } from '@/pages/UnidadPage/components/UnidadDialog'
import { ConfirmDialog } from 'primereact/confirmdialog'
import { desasignarUnidad } from '@/api/unidad.service'
import { Unidad } from '@/model/unidad.model'

export const UnidadesPage = () => {
  const toast = useRef<Toast>(null)
  const { getUnidades, unidadesMateria, setToUpdate } = useStoreUnidad()
  const { materiaSeleccionada } = useStoreMaterias()
  const [mostrarDialog, setMostrarDialog] = useState(false)
  const [mostrarDeleteDialog, setMostrarDeleteDialog] = useState(false)
  const { volver } = useNavigation()
  const [loading, setLoading] = useState(false)
  const [unidadAEliminar, setUnidadAEliminar] = useState<Unidad>(new Unidad())

  useEffect(() => {
    getUnidades(materiaSeleccionada.id)
  }, [])

  const openNew = async () => {
    setMostrarDialog(true)
  }

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Volver" icon="pi pi-arrow-left" className="p-button-danger mr-2" onClick={volver}/>
        </div>
      </>
    )
  }

  const rigthToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button label="Nuevo" icon="pi pi-plus" className="p-button-success mr-2" onClick={async () => await openNew()}/>
        </div>
      </>
    )
  }

  const Footer = (
    (!loading
      ? <div>
        <Button label="Cancelar"
                icon="pi pi-times"
                className="p-button-danger"
                onClick={() => setMostrarDeleteDialog(false)}/>
        <Button label={'Guardar'}
                icon="pi pi-save"
                className="p-button-success"
                onClick={async () => {
                  setLoading(true)
                  await desasignarUnidad(unidadAEliminar.id)
                  await getUnidades(materiaSeleccionada.id)
                  setLoading(false)
                  setMostrarDeleteDialog(false)
                }}
        />
      </div>
      : <div></div>)
  )
  const messageConfirm = {
    message: `¿Desea desasignar la unidad ${unidadAEliminar?.numero}. ${unidadAEliminar?.nombre} de la materia?`,
    header: 'Confirmar eliminación',
    closable: false
  }
  const loadingDialogProps = {
    header: 'Guardando...'
  }

  return (
    <>
      {unidadesMateria && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" left={leftToolbarTemplate} right={rigthToolbarTemplate}></Toolbar>
              <ConfirmDialog
                visible={mostrarDeleteDialog}
                footer={Footer}
                style={{ width: '30rem' }}
                {...(loading ? loadingDialogProps : messageConfirm)}
              />
              <UnidadDialog
                mostrarDialog={mostrarDialog}
                cerrarDialog={() => {
                  setToUpdate(null)
                  setMostrarDialog(false)
                }}/>
            </div>
            <UnidadTable
              openDialog={() => setMostrarDialog(true)}
              openDeleteUnidad={() => setMostrarDeleteDialog(true)}
              setUnidadAEliminar={setUnidadAEliminar}
            />
          </div>
        </div>)}
    </>
  )
}
