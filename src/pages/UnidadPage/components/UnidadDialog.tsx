import { FunctionComponent, useEffect, useRef, useState } from 'react'
import { useStoreMaterias } from '@/zustand/store'
import { Toast } from 'primereact/toast'
import { Button } from 'primereact/button'
import { Controller, useForm } from 'react-hook-form'
import { Unidad } from '@/model/unidad.model'
import { Dialog } from 'primereact/dialog'
import { InputText } from 'primereact/inputtext'
import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { Editor } from 'primereact/editor'
import { actualizarUnidad, asignarUnidad } from '@/api/unidad.service'

export const UnidadDialog: FunctionComponent<any> = ({ mostrarDialog, cerrarDialog }: any) => {
  const toast = useRef<any>(null)
  const { register, handleSubmit, reset, formState: { errors }, control, setValue } = useForm<Unidad>()
  const { materiaSeleccionada } = useStoreMaterias()
  const { setToUpdate, getUnidades, unidadAActualizar } = useStoreUnidad()
  const [isUpdated, setIsUpdated] = useState(false)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (unidadAActualizar) {
      reset({ ...unidadAActualizar })
      setIsUpdated(true)
    } else {
      reset({ ...register })
      setIsUpdated(false)
    }
  }, [mostrarDialog, cerrarDialog])

  const guardarUnidad = async (unidad: Unidad) => {
    try {
      setLoading(true)
      unidad.materiaId = materiaSeleccionada.id
      if (isUpdated) {
        await actualizarUnidad(unidad)
      } else {
        await asignarUnidad(unidad)
      }
      setToUpdate(null)
      reset({ ...register })
      getUnidades(materiaSeleccionada.id)
      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Unidad creada exitosamente', life: 3000 })
      setLoading(false)
      cerrarDialog()
    } catch (error: any) {
      setLoading(false)
      const message = (error as Error).message
      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: message, life: 3000 })
    }
  }

  const dialogFooter = (
    <>
      <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={cerrarDialog}/>
      <Button label={ !isUpdated ? 'Guardar' : 'Actualizar'}
              icon="pi pi-check"
              className="p-button-text"
              onClick={handleSubmit(guardarUnidad)}/>
    </>
  )

  const dialogProps = {
    header: (isUpdated ? 'Actualizar ' : 'Crear ') + 'unidad',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: dialogFooter
  }

  const loadingDialogProps = {
    header: 'Guardando...'
  }

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  return (
    <>
      <Toast ref={toast} />
      <Dialog visible={mostrarDialog}
              style={{ width: '450px' }}
              closable={false}
              className='p-fluid'
              onHide={cerrarDialog}
              {...(loading ? loadingDialogProps : dialogProps)}>
        {loading && <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}></i>}
        <div className={loading ? 'blur' : ''}>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="Nombre" {...register('nombre', { required: 'El nombre es obligatorio' })}/>
            </span>
            {getFormErrorMessage('nombre')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={loading}
                         type="text"
                         autoComplete='false'
                         placeholder="Numero"
                         {...register('numero', { required: 'El numero es obligatorio' })}/>
            </span>
            {getFormErrorMessage('numero')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
                <Controller
                  name="objetivos"
                  control={control}
                  render={({ field: { onChange, value } }) => (
                    <Editor
                      style={{ height: '13rem' }}
                      value={value}
                      readOnly={loading}
                      onTextChange={(e) => onChange(e.htmlValue || '')}/>
                  )}
                />
            </span>
            {getFormErrorMessage('objetivos')}
          </div>
        </div>
      </Dialog>
    </>
  )
}
