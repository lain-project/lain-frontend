import { useStoreUnidad } from '@/zustand/store/unidad.store'
import { useStoreMaterias } from '@/zustand/store'
import { useRef } from 'react'
import { DataTable } from 'primereact/datatable'
import { Spiner } from '@/components/Spiner'
import { Table } from '@/components/Table/Table'
import { Column } from 'primereact/column'
import { Button } from 'primereact/button'
import { Unidad } from '@/model/unidad.model'
import { useNavigate } from 'react-router-dom'

export const UnidadTable = ({ openDialog, openDeleteUnidad, setUnidadAEliminar }: any) => {
  const { unidadesMateria, loading, setToUpdate, setUnidadSeleccionada } = useStoreUnidad()
  const { materiaSeleccionada } = useStoreMaterias()
  const navigate = useNavigate()

  const tableRef = useRef<DataTable>(null)

  const actionBodyTemplate = (unidad: Unidad) => (
    <>
      <Button
        icon="pi pi-pencil"
        className="p-button-rounded p-button-success mr-2"
        tooltip={'Editar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setToUpdate(unidad)
          openDialog()
        }}
      />
      <Button
        icon="pi pi-bookmark"
        className="p-button-rounded p-button-new mr-2"
        tooltip={'Contenido'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setUnidadSeleccionada(unidad)
          navigate('contenido')
        }}
      />
      <Button
        icon="pi pi-trash"
        className="p-button-rounded p-button-danger mr-2"
        tooltip={'Eliminar'}
        tooltipOptions={{ position: 'top' }}
        onClick={() => {
          setUnidadAEliminar(unidad)
          openDeleteUnidad()
        }}
      />
    </>
  )

  return (
    <>
      { loading && <Spiner/> }
      { !loading && (
        <Table tableRef={tableRef}
               data={unidadesMateria}
               title={`Unidades de la materia ${materiaSeleccionada.nombre}`}
               emptyMessage={'No se asigno ninguna unidad a la materia'}
               loading={loading}>
          <Column field="nombre" header="Nombre" sortable></Column>
          <Column field="numero" header="Numero" sortable></Column>
          <Column body={actionBodyTemplate}></Column>
        </Table>
      )}
    </>
  )
}
