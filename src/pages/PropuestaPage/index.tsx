import { useEffect, useRef, useState } from 'react'
import { Toast } from 'primereact/toast'
import { useStoreMaterias } from '@/zustand/store'
import { useNavigate } from 'react-router-dom'
import { actualizarPropuesta, guardarPropuesta, obtenerPropuesta } from '@/api/propuesta.service'
import { Toolbar } from 'primereact/toolbar'
import { Button } from 'primereact/button'
import { Editor } from 'primereact/editor'
import { Propuesta } from '@/model/propuesta.model'

export const PropuestaPage = () => {
  const toast = useRef<Toast>(null)
  const { materiaSeleccionada } = useStoreMaterias()
  const [loading, setLoading] = useState<boolean>(true)
  const navigate = useNavigate()

  const [propuesta, setPropuesta] = useState<Propuesta | null>(null)

  useEffect(() => {
    obtenerPropuesta(materiaSeleccionada.id).then(propuesta => {
      propuesta.materiaId = materiaSeleccionada.id
      setPropuesta(propuesta)
      setLoading(false)
    })
  }, [materiaSeleccionada])

  const guardar = async () => {
    setLoading(true)
    await guardarPropuesta(propuesta)
    const prop = await obtenerPropuesta(materiaSeleccionada.id)
    setPropuesta(prop)
    setLoading(false)
  }

  const actualizar = async () => {
    setLoading(true)
    await actualizarPropuesta(propuesta)
    setLoading(false)
  }

  const rightToolbarTemplate = () => {
    return (
      <>
        {
          propuesta?.id
            ? (
              <div className="my-2">
                <Button label="Actualizar"
                        icon="pi pi-plus"
                        className="p-button-success mr-2"
                        disabled={loading}
                        onClick={actualizar}/>
              </div>)
            : (
              <div className="my-2">
                <Button label="Nuevo"
                        icon="pi pi-plus"
                        className="p-button-success mr-2"
                        disabled={loading}
                        onClick={guardar}/>
              </div>)}

      </>
    )
  }

  const leftToolbarTemplate = () => {
    return (
      <>
        <div className="my-2">
          <Button
            label="Volver"
            icon="pi pi-arrow-left"
            className="p-button-danger mr-2"
            onClick={() => navigate(-1)}/>
        </div>
      </>
    )
  }

  return (
    <>
      {propuesta && (
        <div className="grid crud-demo">
          <div className="col-12">
            <div className="card">
              <Toast ref={toast}/>
              <Toolbar className="mb-4" right={() => rightToolbarTemplate()} left={leftToolbarTemplate}></Toolbar>
            </div>
          </div>
          <div className="col-12">
            <div className="p-datatable">
              <div className="p-datatable-header">
                {loading
                  ? <div className={'flex flex-column align-content-center'}>
                    <h4 className={'align-self-center'}>{propuesta?.id ? 'Guardando...' : 'Actualizando...'}</h4>
                    <div className={'align-self-center'}>
                      <i className="pi pi-spin pi-spinner" style={{ fontSize: '2em' }}/>
                    </div>
                  </div>
                  : <div>
                    <h4>Propuesta de {materiaSeleccionada.nombre}</h4>
                    <hr/>
                    <h5>Contenido Minimo</h5>
                    <hr/>
                    <Editor
                      style={{ height: '8rem' }}
                      value={propuesta.contenidoMinimo || ''}
                      onTextChange={(e) => setPropuesta({ ...propuesta, contenidoMinimo: e.htmlValue })}/>
                    <h5>Fundamentos</h5>
                    <hr/>
                    <Editor
                      style={{ height: '8rem' }}
                      value={propuesta.fundamentos || ''}
                      onTextChange={(e) => setPropuesta({ ...propuesta, fundamentos: e.htmlValue })}/>
                    <h5>Objetivos</h5>
                    <hr/>
                    <Editor
                      style={{ height: '8rem' }}
                      value={propuesta.objetivos || ''}
                      onTextChange={(e) => setPropuesta({ ...propuesta, objetivos: e.htmlValue })}/>
                    <h5>Metodologia</h5>
                    <hr/>
                    <Editor
                      style={{ height: '8rem' }}
                      value={propuesta.metodologia || ''}
                      onTextChange={(e) => setPropuesta({ ...propuesta, metodologia: e.htmlValue })}/>
                    <h5>Evaluacion</h5>
                    <hr/>
                    <Editor
                      style={{ height: '8rem' }}
                      value={propuesta.evaluacion || ''}
                      onTextChange={(e) => setPropuesta({ ...propuesta, evaluacion: e.htmlValue })}/>
                    <h5>Bibliografia General</h5>
                    <hr/>
                    <Editor
                      style={{ height: '8rem' }}
                      value={propuesta.bibliografiaGeneral || ''}
                      onTextChange={(e) => setPropuesta({ ...propuesta, bibliografiaGeneral: e.htmlValue })}/>
                  </div>
                }
              </div>
            </div>
          </div>
        </div>)
      }
    </>
  )
}
