import { Column } from 'primereact/column'
import { DataTable } from 'primereact/datatable'
import { InputText } from 'primereact/inputtext'
import { useEffect, useRef, useState } from 'react'
import { UserProfile } from '@/model/user'
import { useStore } from '@/zustand/store'

export const UsersTable = () => {
  const dt = useRef(null)
  const [selectedUsers, setSelectedUsers] = useState<UserProfile[]>([])
  const [globalFilter, setGlobalFilter] = useState(null)
  const { setUsers, users } = useStore()

  useEffect(() => {
    setUsers()
  }, [])

  const header = (
    <div className="flex flex-column md:flex-row md:justify-content-between md:align-items-center">
      <h5 className="m-0">Usuarios</h5>
      <span className="block mt-2 md:mt-0 p-input-icon-left">
                <i className="pi pi-search"/>
                <InputText type="search" onInput={(e: any) => setGlobalFilter(e.target.value)} placeholder="Buscar..."/>
            </span>
    </div>
  )

  const rolTemplate = (rowData: any) => {
    const color = rowData.rol_name === 'ADMIN' ? 'p-badge-success' : 'p-badge-info'
    return <span className={`p-badge ${color}`}>{rowData.rol_name}</span>
  }

  const statusTemplate = (rowData: any) => {
    const color = rowData.status === 1 ? 'success' : 'danger'
    const msg = rowData.status === 1 ? 'Active' : 'Inactive'
    return <span className={`custom-badge ${color}`}>{msg}</span>
  }

  return (
    <>
      <DataTable ref={dt} value={users} selection={selectedUsers} onSelectionChange={(e) => setSelectedUsers(e.value)}
                 dataKey="id" paginator rows={10} rowsPerPageOptions={[2, 10, 25]}
                 paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                 currentPageReportTemplate="Mostrando {first} al {last} de {totalRecords} usuarios"
                 globalFilter={globalFilter} emptyMessage="No se encontraron usuarios" header={header} responsiveLayout="stack">
        <Column field="username" header="Username" sortable></Column>
        <Column field="email" header="Email" sortable></Column>
        <Column field="firstname" header="Nombres" sortable></Column>
        <Column field="lastname" header="Apellidos" sortable></Column>
        <Column field="rol_name" header="Rol" body={rolTemplate} sortable></Column>
      </DataTable>
    </>
  )
}
