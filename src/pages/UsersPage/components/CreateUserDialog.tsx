import classNames from 'classnames'

import { Button } from 'primereact/button'
import { Dialog } from 'primereact/dialog'
import { Dropdown } from 'primereact/dropdown'
import { InputText } from 'primereact/inputtext'
import { Password } from 'primereact/password'
import { RadioButton } from 'primereact/radiobutton'
import { Divider } from 'primereact/divider'
import { Toast } from 'primereact/toast'

import { useEffect, useRef, useState } from 'react'
import { useForm, SubmitHandler, Controller } from 'react-hook-form'

import { PositionEnum, RolEnum, UserProfile } from '@/model/user'
import { createUser } from '@/api/user.service'
import {useStore} from "@/zustand/store";

export const CreateUserDialog = (props: any) => {
  const toast = useRef<any>(null)
  const { register, handleSubmit, reset, formState: { errors }, control } = useForm<UserProfile>()
  const [user, setUser] = useState<UserProfile>()
  const [userDialog, setUserDialog] = useState(false)
  const emptyUser = {}
  const {setUsers} = useStore()

  const dropdownValues = [
    PositionEnum.TITULAR,
    PositionEnum.ADJUNTO
  ]

  const onSubmit: SubmitHandler<UserProfile> = async data => {
    try {
      await createUser(data)
      await setUsers()
      setUserDialog(false)
      setUser(emptyUser)
      reset({ ...register })
      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Usuario creado exitosamente', life: 3000 })
    } catch (error) {
      const message = (error as Error).message
      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: message, life: 3000 })
    }
  }
  useEffect(() => {
    setUserDialog(props.userDialog)
  }, [props])

  const hideDialog = () => {
    setUser(emptyUser)
    setUserDialog(false)
    reset({ ...register })
    props.onClose()
  }

  const userDialogFooter = (
        <>
            <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={handleSubmit(onSubmit)} />
        </>
  )

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  const usernameValidations = {
    ...register('username', { required: 'Username es requerido' })
  }

  const emailValidations = {
    ...register('email',
      {
        required: 'Email es requerido',
        pattern: {
          value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
          message: 'Direccion de email invalida. Ej. ejemplo@email.com'
        }
      })
  }

  const passwordHeader = <h6>Pick a password</h6>
  const passwordFooter = (
        <>
            <Divider />
            <p className="mt-2">Suggestions</p>
            <ul className="pl-2 ml-2 mt-0" style={{ lineHeight: '1.5' }}>
                <li>At least one lowercase</li>
                <li>At least one uppercase</li>
                <li>At least one numeric</li>
                <li>Minimum 8 characters</li>
            </ul>
        </>
  )

  return (
        <>
            <Toast ref={toast} />
            <Dialog modal={true} visible={userDialog} style={{ width: '450px' }} header="Detalles de usuario" className="p-fluid" onHide={hideDialog} footer={userDialogFooter}>
                <div className="field">
                    <span className="p-input-icon-left">
                        <i className="pi pi-user" />
                        <InputText {...usernameValidations} type="text" autoComplete='false' placeholder="Username" className={classNames({ 'p-invalid': errors.username })} />
                    </span>
                    {getFormErrorMessage('username')}
                </div>
                <div className="field">
                    <span className="p-input-icon-left">
                        <i className="pi pi-envelope" />

                        <InputText type="text" placeholder="Email" {...emailValidations} className={classNames({ 'p-invalid': errors.email })} />
                    </span>
                    {getFormErrorMessage('email')}
                </div>
                <div className="field">
                    <span className="p-input-icon-left">
                        <i className="pi pi-key" />
                        <Controller
                            name="password"
                            control={control}
                            rules={{ required: 'Password es requerido', min: 3 }}
                            render={({ field }) => (
                                <Password {...field}
                                    toggleMask
                                    placeholder="Password"
                                    className={classNames({ 'p-invalid': errors.password })}
                                    header={passwordHeader}
                                    footer={passwordFooter}
                                />
                            )} />
                    </span>
                    {getFormErrorMessage('password')}
                </div>
                <div className="field">
                    <span className="p-input-icon-left">
                        <i className="pi pi-key" />
                        <InputText type="text" placeholder="Nombres" {...register('firstname', { required: 'Nombres es requerido' })} />
                    </span>
                    {getFormErrorMessage('firstname')}
                </div>
                <div className="field">
                    <span className="p-input-icon-left">
                        <i className="pi pi-key" />
                        <InputText type="text" placeholder="Apellidos" {...register('lastname', { required: 'Apellidos es requerido' })} />
                    </span>
                    {getFormErrorMessage('lastname')}
                </div>
                <div className="field">
                    <label className="mb-3">Rol <br />{getFormErrorMessage('rolId')}</label>
                    <Controller
                        name="rolId"
                        control={control}
                        rules={{ required: 'Rol es requerido' }}
                        render={({ field: { onChange, value } }) => (
                            <div className="formgrid grid">
                                <div className="field-radiobutton col-12">
                                    <RadioButton inputId="category1" value={1} onChange={onChange} checked={value === RolEnum.ADMIN} />
                                    <label htmlFor="category1">Admin</label>
                                </div>
                                <div className="field-radiobutton col-12">
                                    <RadioButton inputId="category2" value={2} onChange={onChange} checked={value === RolEnum.DOCENTE} />
                                    <label htmlFor="category2">Docente</label>
                                </div>
                            </div>)
                        } />
                </div>
                <div className="field">
                    <label className="mb-3">Posicion</label>
                    <Controller
                        name="position"
                        control={control}
                        render={({ field: { onChange, value } }) => (
                            <Dropdown value={value} showClear={true} onChange={onChange} options={dropdownValues} placeholder="Elegir" />
                        )} />
                </div>
            </Dialog>
        </>
  )
}
