import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { useRef, useState } from 'react'
import { User } from '@/model/user'
import { CreateUserDialog } from './components/CreateUserDialog'
import { UsersTable } from './components/UsersTable'
import { ButtonNew } from '@/components/ButtonNew'

export const UsersPage: React.FC = () => {
  const toast = useRef<Toast>(null)
  const [userDialog, setUserDialog] = useState(false)

  const openNew = () => {
    setUserDialog(true)
  }

  const newUser = (<ButtonNew onClick={openNew}/>)

  return (
        <>
            <div className="grid crud-demo">
                <div className="col-12">
                    <div className="card">
                        <Toast ref={toast} />
                        <Toolbar
                          className="mb-4"
                          right={newUser}/>
                        <CreateUserDialog userDialog={userDialog} onClose={() => setUserDialog(false)}/>
                    </div>
                    <UsersTable />
                </div>
            </div>
        </>
  )
}
