import { useEffect, useRef, useState } from 'react'
import { SubmitHandler, useForm } from 'react-hook-form'
import { StudyPlanModel } from '@/model/study-plan.model'
import { createStudyPlan, updateStudyPlan } from '@/api/study-plan.service'
import { Button } from 'primereact/button'
import { Dialog } from 'primereact/dialog'
import { InputText } from 'primereact/inputtext'
import { InputTextarea } from 'primereact/inputtextarea'
import { Toast } from 'primereact/toast'
import { useStoreStudyPlan } from '@/zustand/store'

export const StudyPlanDialog: React.FC<any> = (props: any) => {
  const { getStudyPlans, studyPlanToUpdate, setStudyPlanToUpdate } = useStoreStudyPlan()
  const toast = useRef<any>(null)
  const { register, handleSubmit, reset, formState: { errors }, control, setValue } = useForm<StudyPlanModel>()
  const [isloading, setIsloading] = useState(false)

  const [studyPlan, setStudyPlan] = useState<StudyPlanModel>()
  const [isUpdated, setIsUpdated] = useState(false)
  const [studyPlanDialog, setStudyPlanDialog] = useState(false)
  const emptyStudyPlan = {}

  useEffect(() => {
    setStudyPlanDialog(props.studyPlanDialog)
    if (studyPlanToUpdate) {
      console.log('studyPlanToUpdate', studyPlanToUpdate)
      setIsUpdated(true)
      setValue('id', studyPlanToUpdate.id)
      setValue('code', studyPlanToUpdate.code)
      setValue('years', studyPlanToUpdate.years)
      setValue('name', studyPlanToUpdate.name)
      setValue('description', studyPlanToUpdate.description)
    } else {
      console.log('no update')
      setIsUpdated(false)
      reset({ ...register })
    }
  }, [props])

  const onSubmit: SubmitHandler<StudyPlanModel> = async data => {
    try {
      console.log(data)
      setIsloading(true)
      if (isUpdated) {
        await updateStudyPlan(data)
      } else {
        await createStudyPlan(data)
      }
      setStudyPlan(emptyStudyPlan)
      setStudyPlanToUpdate(null)
      reset({ ...register })
      setIsloading(false)
      hideDialog()
      await getStudyPlans()
      setStudyPlanDialog(false)
      toast.current.show({ severity: 'success', sumary: 'Exito', detail: 'Plan de estudio creado exitosamente', life: 3000 })
    } catch (error) {
      setIsloading(false)
      hideDialog()
      console.log(error)
      toast.current.show({ severity: 'error', sumary: 'Fallo', detail: error, life: 3000 })
    }
  }

  const hideDialog = () => {
    setStudyPlan(emptyStudyPlan)
    setStudyPlanDialog(false)
    setStudyPlanToUpdate(null)
    reset({ ...register })
    props.onClose()
  }

  const studyPlanDialogFooter = (
    <>
      <Button label="Cancelar" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
      <Button label="Guardar" icon="pi pi-check" className="p-button-text" onClick={handleSubmit(onSubmit)} />
    </>
  )

  const getFormErrorMessage = (name: any) => {
    const _errors = errors as any[]
    return _errors[name] && <small className="p-error">{_errors[name].message}</small>
  }

  const dialogProps = {
    header: (isUpdated ? 'Modificar ' : 'Crear ') + 'plan de estudio',
    breakpoints: { '960px': '75vw', '640px': '100vw' },
    footer: studyPlanDialogFooter
  }

  const loadingDialogProps = {
    header: 'Guardando...'
  }

  const closable = false

  return (
    <>
      <Toast ref={toast} />
      <Dialog visible={studyPlanDialog}
              style={{ width: '450px' }}
              closable={closable}
              className='p-fluid'
              {...(isloading ? loadingDialogProps : dialogProps)}
               onHide={hideDialog}>
        {isloading && <i className="pi pi-spin pi-spinner spinner_overlay" style={{ fontSize: '2em' }}></i>}
        <div className={isloading ? 'blur' : ''}>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText disabled={isloading} type="text" autoComplete='false' placeholder="Código" {...register('code', { required: 'El código es obligatorio' })}/>
            </span>
            {getFormErrorMessage('code')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText type="text" disabled={isloading} autoComplete='false' placeholder="Nombre" {...register('name', { required: 'El nombre es obligatorio' })}/>
            </span>
            {getFormErrorMessage('name')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
              <InputText type="text" disabled={isloading} autoComplete='false' placeholder="Años" {...register('years', { required: 'La cantidad de años es obligario' })}/>
            </span>
            {getFormErrorMessage('years')}
          </div>
          <div className="field">
            <span className="p-input-icon-left">
              <InputTextarea disabled={isloading} autoComplete='false' placeholder="Descripción" {...register('description')}/>
            </span>
          </div>
        </div>
      </Dialog>
      </>
  )
}
