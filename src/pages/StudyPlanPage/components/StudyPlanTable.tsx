import { DataTable } from 'primereact/datatable'
import { Button } from 'primereact/button'
import { Column } from 'primereact/column'
import { useRef } from 'react'
import { Table } from '@/components/Table/Table'
import { useStoreStudyPlan } from '@/zustand/store'
import { useNavigate } from 'react-router-dom'
import { StudyPlanModel } from '@/model/study-plan.model'
import { useRol } from '@/hooks/useRol.hook'

export const StudyPlanTable = ({ openDialog }: any) => {
  const navigate = useNavigate()
  const { isAdmin } = useRol()
  const { studysPlans, loading, setStudyPlanToUpdate } = useStoreStudyPlan()

  const tableRef = useRef<DataTable>(null)

  const navigateToDetail = (studyPlan: StudyPlanModel) => {
    navigate(`/study-plans/${studyPlan.id}`)
  }

  const actionBodyTemplate = (rowData: any) => {
    return (
      <div className="actions">
        {
          isAdmin() && (
            <Button icon="pi pi-pencil" className="p-button-rounded p-button-success mr-2"
                    tooltipOptions={{ position: 'top' }}
                    tooltip={'Modificar'}
                    onClick={() => {
                      setStudyPlanToUpdate(rowData)
                      openDialog()
                    }}/>
          )
        }

        <Button icon="pi pi-list" className="p-button-rounded p-button-danger mr-2"
                tooltipOptions={{ position: 'top' }}
                tooltip={'Materias'}
                onClick={() => {
                  navigateToDetail(rowData)
                }}/>
      </div>
    )
  }

  return (
    <>
      { studysPlans && (
      <Table tableRef={tableRef}
             data={studysPlans} title='Planes de estudio'
             loading={loading} footerTitle='planes'>
        <Column field="code" header="Código" sortable></Column>
        <Column field="name" header="Nombre" sortable></Column>
        <Column field="description" header="Descripción" sortable></Column>
        <Column body={actionBodyTemplate}></Column>
      </Table>)}
    </>
  )
}
