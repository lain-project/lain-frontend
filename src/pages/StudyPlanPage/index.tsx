import { Toast } from 'primereact/toast'
import { Toolbar } from 'primereact/toolbar'
import { useEffect, useRef, useState } from 'react'
import { StudyPlanTable } from './components/StudyPlanTable'
import { StudyPlanDialog } from '@/pages/StudyPlanPage/components/StudyPlanDialog'
import { useRol } from '@/hooks/useRol.hook'
import { ButtonNew } from '@/components/ButtonNew'
import { useStore, useStoreStudyPlan } from '@/zustand/store'

export const StudyPlanPage = () => {
  const toast = useRef<Toast>(null)
  const { isAdmin } = useRol()
  const { user } = useStore()
  const [studyPlanDialog, setStudyPlanDialog] = useState<boolean>(false)
  const { getStudyPlans, getPlanesAsignados } = useStoreStudyPlan()

  useEffect(() => {
    if (isAdmin()) {
      getStudyPlans()
    } else {
      getPlanesAsignados(user.id)
    }
  }, [])

  const openNew = () => setStudyPlanDialog(true)

  const newStudyPlan = (<ButtonNew onClick={openNew}/>)

  const headerAdmin = (
    <div className="card">
      <Toast ref={toast} />
      <Toolbar className="mb-4" right={newStudyPlan}></Toolbar>
      <StudyPlanDialog studyPlanDialog={studyPlanDialog} onClose={() => setStudyPlanDialog(false)}/>
    </div>
  )

  const headerDocente = (
    <>
      <Toast ref={toast} />
      <StudyPlanDialog studyPlanDialog={studyPlanDialog} onClose={() => setStudyPlanDialog(false)}/>
    </>
  )

  return (
        <>
            <div className="grid crud-demo">
                <div className="col-12">
                  {isAdmin() ? headerAdmin : headerDocente}
                  <StudyPlanTable openDialog={openNew} />
                </div>
            </div>
        </>
  )
}
