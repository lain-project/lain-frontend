import { AppRouter } from './routes/AppRouter';
import { useStore } from './zustand/store';
import {useEffect, useState} from 'react';
import jwtDecode from 'jwt-decode';
import { UserStore } from './model/user';
import { Token } from './model/token';

const init = (setUser: any, setToken: any) => {
  const accessToken = localStorage.getItem('token') || '';
  const tokenNeuralactions = localStorage.getItem('tokenneuralaction') || '';
  const token = new Token();
  token.accessToken = accessToken;
  token.tokenNeuralaction = tokenNeuralactions;
  try {
    const tokenData = jwtDecode(accessToken) as any || {};
    const user = tokenData.user as UserStore;
    user.logged = true;
    setUser(user);
    setToken(token)

  } catch (error) {
    console.log(error)
  }
  return token || '';
}

export default function App() {
  const [isLoading, setIsLoading] = useState(true)
  const { setUser, setToken } = useStore();
  useEffect(() => {
    init(setUser, setToken)
    setIsLoading(false)

  }, [])
  return (
    <>
      {!isLoading && <AppRouter />}
    </>
  )
}

