import { useStore } from '@/zustand/store'
import { useState } from 'react'
import { Rolname } from '@/model/user'

export const useRol = () => {
  const { user } = useStore()
  const [rol] = useState(user.rolname)

  const isAdmin = () => rol === Rolname.ADMIN

  return { rol, isAdmin }
}
