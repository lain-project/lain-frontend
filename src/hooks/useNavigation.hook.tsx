import { useNavigate } from 'react-router-dom'

export const useNavigation = () => {
  const navigate = useNavigate()

  const volver = () => {
    navigate(-1)
  }

  return {volver}
}
