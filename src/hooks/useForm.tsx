import { useState } from 'react'

export const useForm = (initialState = {}) => {
  const [values, setValues] :any = useState(initialState)

  const restart = (newState: any) => {
    setValues(newState)
  }

  const reset = () => {
    setValues(initialState)
  }

  const handleInputChange = ({ target }: any) => {
    setValues({
      ...values,
      [target.name]: target.value
    })
  }

  return [values, handleInputChange, reset, restart]
}
