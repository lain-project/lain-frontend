import { MateriaRest } from '@/api/models/materia.rest'
import { Materia } from '@/model/materia'

export const materiaAdapterToDomain = (materiaRest: MateriaRest): Materia => ({
  id: materiaRest.id,
  nombre: materiaRest.nombre,
  codigo: materiaRest.codigo,
  regimen: materiaRest.regimen,
  anio: materiaRest.anio,
  horasSemanales: materiaRest.horas_semanales,
  horasTotales: materiaRest.horas_totales
})

export const materiaAdapterToRest = (materia: Materia): MateriaRest => ({
  nombre: materia.nombre,
  codigo: materia.codigo,
  regimen: materia.regimen,
  anio_plan_de_estudio: Number(materia.anioPlanEstudio),
  id: materia?.id,
  horas_semanales: Number(materia?.horasSemanales),
  horas_totales: Number(materia?.horasTotales)
})
