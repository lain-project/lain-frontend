import { StudyPlanRest } from '@/api/models/study-plan.rest'
import { StudyPlanModel } from '@/model/study-plan.model'

export const studyPlanAdapter = (studyPlanRest: StudyPlanRest): StudyPlanModel => ({
  id: studyPlanRest.id,
  years: studyPlanRest.years,
  code: studyPlanRest.code,
  name: studyPlanRest.name,
  description: studyPlanRest.description
})

export const planDeEstudioAdapter = (data: any): StudyPlanModel => ({
  id: data.id,
  years: null,
  code: data.codigo,
  name: data.nombre,
  description: data.descripcion
})
