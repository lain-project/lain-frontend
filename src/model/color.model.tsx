export const colors = [
  '#C5E1A5',
  '#64B5F6',
  '#C278CE',
  '#F48FB1',
  '#FFE082',
  '#78909C',
  '#78cc86',
  '#8dd0ff',
  '#b7a2e0',
  '#f19ea6',
  '#ffe082',
  '#6c757d',
  '#86EFAC',
  '#A5B4FC',
  '#D8B4FE',
  '#FCA5A5',
  '#FCD34D',
  '#CBD5E1'
]
