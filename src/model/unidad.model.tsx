export class Unidad {
  id?: string;
  nombre?: string;
  numero?: number;
  objetivos?: string;
  materiaId?: string;
}
