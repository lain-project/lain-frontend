export class Anio {
    id?: number;
    numero?: number;
    nombre?: string;
    planEstudioId?: number;
}
