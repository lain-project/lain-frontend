export class Config {
    rippleEffect?: boolean;
    onRippleEffect: (e: any) => void = () => { };
    inputStyle?: string;
    onInputStyleChange: (e: any) => void = () => {};
    layoutMode?: string;
    onLayoutModeChange: (e: any) => void = () => { };
    layoutColorMode?: string;
    onColorModeChange: (e: any) => void = () => { };

}


export class TopMenuOptions{
    onToggleMenuClick: (e: any) => void = (e:any) => { };
    layoutColorMode?: string;
    mobileTopbarMenuActive?: boolean;
    onMobileTopbarMenuClick: (e: any) => void = (e:any) => { };
    onMobileSubTopbarMenuClick: (e: any) => void = (e:any) => { };

}
