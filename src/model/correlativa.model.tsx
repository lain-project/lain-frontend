import {Materia} from "@/model/materia";

export enum CorrelativaCursar {
  NO_APLICA = 0,
  REGULARIZADA = 1,
  APROBADA = 2,
}

export enum CorrelativaFinal {
  NO_APLICA = 0,
  APROBADA = 1,
}

export class Correlativa {
  id?: string;
  correlativaId?: string;
  materiaId?: string;
  necesariaParaCursar?: CorrelativaCursar;
  necesariaParaRendirFinal?: CorrelativaFinal;
  correlativa?: Materia;
  materia?: Materia;
}
