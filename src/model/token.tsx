export class Token {
    accessToken = '';
    tokenNeuralaction = '';
    refreshToken?: string = '';

  // constructor(accessToken?: string, tokenNeuralaction?: string, refreshToken?: string){
  //     this.accessToken = accessToken;
  //     this.tokenNeuralaction = tokenNeuralaction;
  //     this.refreshToken = refreshToken;
  // }
}

export class TokenMapper {
  static toToken (tokenRest: any): Token {
    const token = new Token()
    token.accessToken = tokenRest.access_token
    token.refreshToken = tokenRest.refresh_token
    token.tokenNeuralaction = tokenRest.token_neuralaction
    return token
  }
}
