export class StudyPlanModel {
  id?: number
  code?: string
  years?: number | null
  name?: string
  description?: string
}
