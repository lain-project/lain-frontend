export enum Regimen {
  ANUAL = 'ANUAL',
  CUATRIMESTRAL = 'CUATRIMESTRAL',
}

export class Materia {
  id?: number
  codigo?: string
  anio?: number
  nombre?: string
  regimen?: Regimen | null
  anioPlanEstudio?: number | null
  horasSemanales?: number
  horasTotales?: number
}
