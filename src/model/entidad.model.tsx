export class Entidad {
  synonyms?: string[];
  value?: string;
}

export class TipoEntidad {
  title?: string;
  entities?: Entidad[];
  name?: string;
  displayName?: string;
  kind?: string;
  autoExpansionMode?: string;
  enableFuzzyExtraction?: boolean;
}
