export class Contenido {
  id?: string;
  nombre?: string;
  descripcion?: string;
  esMinimo?: boolean;
  unidadId?: string;
}
