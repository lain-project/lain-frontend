export class User {
    username?: string;
    password?: string;
}

export class UserStore {
  id?: number;
  username?: string;
  email?:string;
  avatar?: string;
  rolname?: string;
  google?: boolean;
  logged = false;
}

export class UserProfile extends User {
    id?: number;
    email?: string;
    firstname?: string;
    lastname?: string;
    position?: string;
    avatar?: string;
    rolId?: number;
    status?: number;
    rolName?: string;
}

export enum RolEnum {
    ADMIN = 1,
    DOCENTE = 2
}

export enum PositionEnum {
    TITULAR = 'Titular',
    ADJUNTO = 'Adjunto',
}

export enum Rolname {
  ADMIN='ADMIN',
  DOCENTE='DOCENTE'
}
