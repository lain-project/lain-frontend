export class Perfil {
  id?: number;
  apellidos?: string;
  nombres?: string;
  cargo?: string;
}

export class Docente {
  id?: number;
  username?: string;
  email?: string;
  estado?: number;
  perfil?: Perfil;
}
