export class Propuesta {
  id?: string;
  contenidoMinimo?: string = '';
  fundamentos?: string = '';
  objetivos?: string = '';
  metodologia?: string = '';
  evaluacion?: string = '';
  bibliografiaGeneral?: string = '';
  materiaId?: string | null;
}
