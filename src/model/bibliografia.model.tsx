export class Bibliografia {
  id?: string;
  nombre?: string;
  isbn?: string;
  autor?: string;
  materiaId?: string
}
