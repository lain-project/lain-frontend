/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly VITE_APP_BASE_URL?: string;
  readonly VITE_APP_BASE_URL_NEST?: string;
  readonly VITE_APP_BASE_URL_CHATBOT?: string;
  readonly VITE_GOOGLE_CLIENT_ID?: string;
  readonly VITE_TINY_CLOUD_API_KEY?: string;
  // more env variables...
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
