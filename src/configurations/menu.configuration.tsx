import { Rolname } from '@/model/user'

interface Menu {
  label: string
  icon: string
  to: string
  roles: Rolname[]
}

const usuario = {
  label: 'Usuarios',
  icon: 'pi pi-fw pi-users',
  to: '/users',
  roles: [Rolname.ADMIN]
}

const planDeEstudio = {
  label: 'Planes de estudio',
  icon: 'pi pi-fw pi-map',
  to: '/study-plans',
  roles: [Rolname.ADMIN, Rolname.DOCENTE]
}

const chatbot = {
  label: 'ChatBot',
  icon: 'pi pi-fw pi-discord',
  to: '/chatbot',
  roles: [Rolname.ADMIN]
}

const reportes = {
  label: 'Reportes',
  icon: 'pi pi-fw pi-chart-bar',
  to: '/reportes',
  roles: [Rolname.ADMIN]
}

export class MenuOption {
  private opciones: any[];

  constructor () {
    this.opciones = [usuario, planDeEstudio, chatbot, reportes]
  }

  get admin (): any[] {
    const options = this.opciones.filter(menu => menu.roles.includes(Rolname.ADMIN))
    return [{
      items: options
    }]
  }

  get docente (): any[] {
    const options = this.opciones.filter(menu => menu.roles.includes(Rolname.DOCENTE))
    return [{
      items: options
    }]
  }
}
