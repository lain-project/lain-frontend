import { CSSTransition } from 'react-transition-group'
import { useLocation } from 'react-router-dom'
import { Tooltip } from 'primereact/tooltip'
import { useState, useEffect, useRef } from 'react'
import classNames from 'classnames'
import PrimeReact from 'primereact/api'

import { ConfigPanel } from '../components/ConfigPanel'
import { Footer } from '../components/Footer'
import { SidebarMenu } from '../components/SidebarMenu'
import { TopMenu } from '../components/TopMenu'

import 'primereact/resources/primereact.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'
import 'prismjs/themes/prism-coy.css'
import '../assets/demo/flags/flags.css'
import '../assets/demo/Demos.scss'
import '../assets/layout/layout.scss'
import '../App.scss'
import { useStore } from '@/zustand/store'
import { MenuOption } from '@/configurations/menu.configuration'
import { Rolname } from '@/model/user'

const DashboardLayout = ({ children }: any) => {
  const [layoutMode, setLayoutMode] = useState('static')
  const [layoutColorMode, setLayoutColorMode] = useState('dark')
  const [inputStyle, setInputStyle] = useState('outlined')
  const [ripple, setRipple] = useState(true)
  const [staticMenuInactive, setStaticMenuInactive] = useState(false)
  const [overlayMenuActive, setOverlayMenuActive] = useState(false)
  const [mobileMenuActive, setMobileMenuActive] = useState(false)
  const [mobileTopbarMenuActive, setMobileTopbarMenuActive] = useState(false)
  const copyTooltipRef = useRef<Tooltip | any>(null)
  const location = useLocation()
  const { user } = useStore()
  const [menu, setMenu] = useState<any[]>([])

  PrimeReact.ripple = true

  let menuClick = false
  let mobileTopbarMenuClick = false

  useEffect(() => {
    if (mobileMenuActive) {
      addClass(document.body, 'body-overflow-hidden')
    } else {
      removeClass(document.body, 'body-overflow-hidden')
    }
    const menu = new MenuOption()
    user.rolname === Rolname.ADMIN ? setMenu(menu.admin) : setMenu(menu.docente)
  }, [mobileMenuActive])

  useEffect(() => {
    copyTooltipRef && copyTooltipRef.current && copyTooltipRef.current.updateTargetEvents()
  }, [location])

  const onInputStyleChange = (inputStyle: string) => {
    setInputStyle(inputStyle)
  }

  const onRipple = (e: any) => {
    PrimeReact.ripple = e.value
    setRipple(e.value)
  }

  const onLayoutModeChange = (mode: string) => {
    setLayoutMode(mode)
  }

  const onColorModeChange = (mode: string) => {
    setLayoutColorMode(mode)
  }

  const onWrapperClick = () => {
    if (!menuClick) {
      setOverlayMenuActive(false)
      setMobileMenuActive(false)
    }

    if (!mobileTopbarMenuClick) {
      setMobileTopbarMenuActive(false)
    }

    mobileTopbarMenuClick = false
    menuClick = false
  }

  const onToggleMenuClick = (event: PointerEvent) => {
    menuClick = true

    if (isDesktop()) {
      if (layoutMode === 'overlay') {
        if (mobileMenuActive === true) {
          setOverlayMenuActive(true)
        }
        setOverlayMenuActive((prevState) => !prevState)
        setMobileMenuActive(false)
      } else if (layoutMode === 'static') {
        setStaticMenuInactive((prevState) => !prevState)
      }
    } else {
      setMobileMenuActive((prevState) => !prevState)
    }

    event.preventDefault()
  }

  const onSidebarClick = () => menuClick = true

  const onMobileTopbarMenuClick = (event: any) => {
    console.log('onMobileTopbarMenuClick')
    mobileTopbarMenuClick = true

    setMobileTopbarMenuActive((prevState) => !prevState)
    event.preventDefault()
  }

  const onMobileSubTopbarMenuClick = (event: any) => {
    console.log('onMobileSubTopbarMenuClick')
    mobileTopbarMenuClick = true

    event.preventDefault()
  }

  const onMenuItemClick = (event: any) => {
    if (!event.item.items) {
      setOverlayMenuActive(false)
      setMobileMenuActive(false)
    }
  }

  const isDesktop = () => window.innerWidth >= 992

  const addClass = (element: HTMLElement, className: string) => {
    if (element.classList) { element.classList.add(className) } else { element.className += ' ' + className }
  }

  const removeClass = (element: HTMLElement, className: string) => {
    if (element.classList) { element.classList.remove(className) } else { element.className = element.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ') }
  }

  const wrapperClass = classNames('layout-wrapper', {
    'layout-overlay': layoutMode === 'overlay',
    'layout-static': layoutMode === 'static',
    'layout-static-sidebar-inactive': staticMenuInactive && layoutMode === 'static',
    'layout-overlay-sidebar-active': overlayMenuActive && layoutMode === 'overlay',
    'layout-mobile-sidebar-active': mobileMenuActive,
    'p-input-filled': inputStyle === 'filled',
    'p-ripple-disabled': ripple === false,
    'layout-theme-light': layoutColorMode === 'light'
  })

  return (
        <div className={wrapperClass} onClick={onWrapperClick}>
            <Tooltip ref={copyTooltipRef} target=".block-action-copy" position="bottom" content="Copied to clipboard" event="focus" />

            <TopMenu onToggleMenuClick={onToggleMenuClick} layoutColorMode={layoutColorMode}
                mobileTopbarMenuActive={mobileTopbarMenuActive} onMobileTopbarMenuClick={onMobileTopbarMenuClick} onMobileSubTopbarMenuClick={onMobileSubTopbarMenuClick}/>

            <div className="layout-sidebar" onClick={onSidebarClick}>
                <SidebarMenu model={menu} onMenuItemClick={onMenuItemClick} layoutColorMode={layoutColorMode} />
            </div>

            <div className="layout-main-container">
                <div className="layout-main">
                    {children}
                </div>

                <Footer layoutColorMode={layoutColorMode} />
            </div>

            <ConfigPanel
                rippleEffect={ripple} onRippleEffect={onRipple}
                inputStyle={inputStyle} onInputStyleChange={onInputStyleChange}
                layoutMode={layoutMode} onLayoutModeChange={onLayoutModeChange}
                layoutColorMode={layoutColorMode} onColorModeChange={onColorModeChange}
            />

            <CSSTransition classNames="layout-mask" timeout={{ enter: 200, exit: 200 }} in={mobileMenuActive} unmountOnExit>
                <div className="layout-mask p-component-overlay"></div>
            </CSSTransition>

        </div>
  )
}
export default DashboardLayout
