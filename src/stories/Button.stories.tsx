import { LoginButton } from "../components/LoginButton"

export default {
    title: 'Components/Button',
    component: LoginButton,
    argTypes: {
        handleClick: { action: 'clicked' },
    }
}

const Template = (args: any) => <LoginButton {...args} />


export const Login: any = Template.bind({})

Login.args = {
    label: 'Login',
    type: 'submit',
}