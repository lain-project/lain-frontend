import { UserStore } from '@/model/user'
import { Token } from '@/model/token'
import { devtools } from 'zustand/middleware'
import create from 'zustand'
import { getStudyPlans, obtenerPlanesDeEstudioAsignados } from '@/api/study-plan.service'
import { studyPlanAdapter } from '@/adapters/study-plan.adapter'
import { StudyPlanModel } from '@/model/study-plan.model'
import { obtenerMateriasPlanEstudio } from '@/api/materia.services'
import { Materia } from '@/model/materia'
import { materiaAdapterToDomain } from '@/adapters/materia.adapter'
import { obtenerAniosPlanDeEstudio } from '@/api/anio.service'
import {getUsers} from "@/api/user.service";

interface MyState {
  users: UserStore[]
  user: UserStore
  token: Token
  setUser: (user: UserStore) => void
  setLogin: (logged: boolean) => void
  setToken: (token: Token) => void
  setUsers: () => Promise<void>
}

export const useStore = create<MyState>(
  devtools(
    (set) => ({
      users: [],
      user: new UserStore(),
      token: new Token(),
      setUser: (user: UserStore) => set(state => ({ ...state, user })),
      setLogin: (logged: boolean) => set((state) => ({
        user: { ...state.user, logged }
      })),
      setToken: (token: Token) => set(() => ({
        token: token
      })),
      setUsers: async () => {
        const users = await getUsers()
        set({users})
      }
    })
  )
)

interface StudyPlanState {
  studysPlans: StudyPlanModel[]
  studyPlanToUpdate: StudyPlanModel | null
  loading: boolean
  error: any
  getStudyPlans: () => Promise<any>
  setStudyPlanToUpdate: (studyPlan: StudyPlanModel | null) => void
  getPlanesAsignados: (id: number | undefined) => Promise<any>
}

// const timeout = (ms) => new Promise(resolve => setTimeout(resolve, ms))
//
// async function sleep (fn, ...args) {
//   await timeout(30000)
//   return fn(...args)
// }

export const useStoreStudyPlan = create<StudyPlanState>(
  devtools(
    (set) => ({
      studysPlans: [],
      studyPlanToUpdate: null,
      loading: false,
      error: null,
      getStudyPlans: async () => {
        try {
          set({ loading: true })
          const studysPlans = await getStudyPlans()
          const result: StudyPlanModel[] = studysPlans.map(sp => studyPlanAdapter(sp))
          set({ studysPlans: result, loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setStudyPlanToUpdate: (studyPlan: StudyPlanModel | null) => set(() => ({
        studyPlanToUpdate: studyPlan
      })),
      getPlanesAsignados: async (idUsuario) => {
        try {
          set({ loading: true })
          const studysPlans = await obtenerPlanesDeEstudioAsignados(idUsuario)
          set({ studysPlans, loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      }
    })
  )
)

export const useStoreMaterias = create<any>(
  devtools(
    (set) => ({
      materias: [],
      materiaActualizar: null,
      materiaSeleccionada: null,
      loading: false,
      error: null,
      getMaterias: async (idPlanDeEstudio: number) => {
        try {
          set({ loading: true })
          const materias = await obtenerMateriasPlanEstudio(idPlanDeEstudio)
          const result: Materia[] = materias.map(materia => materiaAdapterToDomain(materia))
          set({ materias: result, loading: false })
          return result
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setMateriaActualizar: (materia: Materia | null) => set(() => ({
        materiaActualizar: materia
      })),
      setMateriaSeleccionada: (materia: Materia | null) => set(() => ({
        materiaSeleccionada: materia
      }))
    })
  )
)

export const useStoreAnios = create<any>(
  devtools(
    (set) => ({
      anios: [],
      loading: false,
      error: null,
      obtenerAnios: async (idPlanDeEstudio: number) => {
        try {
          set({ loading: true })
          const anios = await obtenerAniosPlanDeEstudio(idPlanDeEstudio)
          set({ anios, loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      }
    })
  )
)
