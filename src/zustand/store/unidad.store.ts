import create from 'zustand'
import { devtools } from 'zustand/middleware'
import { obtenerUnidades } from '@/api/unidad.service'
import { Unidad } from '@/model/unidad.model'

export const useStoreUnidad = create<any>(
  devtools(
    (set) => ({
      unidadesMateria: [],
      unidadAActualizar: null,
      unidadSeleccionada: null,
      loading: false,
      error: null,
      getUnidades: async (idMateria: string) => {
        try {
          set({ loading: true })
          const unidadesMateria = await obtenerUnidades(idMateria)
          set({ unidadesMateria })
          set({ loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setUnidades: async (unidadesMateria: Unidad[]) => set({
        unidadesMateria
      }),
      setToUpdate: (unidadAActualizar: Unidad) => set({ unidadAActualizar }),
      setUnidadSeleccionada: (unidadSeleccionada: Unidad) => set({ unidadSeleccionada })
    })
  )
)
