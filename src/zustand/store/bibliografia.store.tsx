import create from 'zustand'
import { devtools } from 'zustand/middleware'
import { obtenerBibliografia } from '@/api/bibliografia.service'
import { Bibliografia } from '@/model/bibliografia.model'

interface BibliografiaStore {
  bibliografiaMateria: Bibliografia[]
  bibliografiaAActualizar: Bibliografia | null
  bibliografiaSeleccionada: Bibliografia | null
  loading: boolean
  error: any
  getBibliografia: (idMateria: string) => Promise<void>
  setBibliografias: (bibliografias: Bibliografia[]) => Promise<void>
  setToUpdate: (bibliografiaAActualizar: Bibliografia | null) => void
  setBibliografiaSeleccionada: (bibliografiaSeleccionada: Bibliografia | null) => void
}

export const useStoreBibliografia = create<BibliografiaStore>(
  devtools(
    (set) => ({
      bibliografiaMateria: [],
      bibliografiaAActualizar: null,
      bibliografiaSeleccionada: null,
      loading: false,
      error: null,
      getBibliografia: async (idMateria: string) => {
        try {
          set({ loading: true })
          const bibliografiaMateria = await obtenerBibliografia(idMateria)
          set({ bibliografiaMateria })
          set({ loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setBibliografias: async (bibliografiaMateria: Bibliografia[]) => set({
        bibliografiaMateria
      }),
      setToUpdate: (bibliografiaAActualizar: Bibliografia | null) => set({ bibliografiaAActualizar }),
      setBibliografiaSeleccionada: (bibliografiaSeleccionada: Bibliografia | null) => set({ bibliografiaSeleccionada })
    })
  )
)
