import create from 'zustand'
import { devtools } from 'zustand/middleware'
import {getDocentesMateria, obtenerDocentesNoAsignados} from '@/api/docente.service'
import {Docente} from "@/model/docente.model";

export const useStoreDocente = create<any>(
  devtools(
    (set) => ({
      docentesMateria: [],
      docentesNoAsignadosMateria: [],
      loading: false,
      error: null,
      getDocentes: async (idMateria: string) => {
        try {
          set({ loading: true })
          const docentesMateria = await getDocentesMateria(idMateria)
          set({ docentesMateria, loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      getDocentesNoAsignados: async (idMateria: string) => {
        try {
          set({ loading: true })
          const docentesNoAsignadosMateria = await obtenerDocentesNoAsignados(idMateria)
          set({ docentesNoAsignadosMateria, loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setLoading: (loading: boolean) => set(() => ({
        loading
      })),
      setDocentes: (docentesMateria: Docente[]) => set(() => ({
        docentesMateria
      }))
    })
  )
)
