import create from 'zustand'
import { devtools } from 'zustand/middleware'
import { Correlativa } from '@/model/correlativa.model'
import { obtenerCorrelativas, obtenerMateriasCorrelativas } from '@/api/correlativa.service'
import { Materia } from '@/model/materia'

interface CorrelativaStore {
  correlativasMateria: Correlativa[]
  correlativaAActualizar: Correlativa | null
  correlativaSeleccionada: Correlativa | null
  materiasCorrelativas: Materia[]
  loading: boolean
  error: any
  getCorrelativas: (idMateria: string) => Promise<void>
  getMateriasCorrelativas: (idMateria: string) => Promise<void>
  setCorrelativas: (correlativasMateria: Correlativa[]) => Promise<void>
  setToUpdate: (correlativaAActualizar: Correlativa | null) => void
  setCorrelativaSeleccionada: (correlativaSeleccionada: Correlativa | null) => void
}

export const useStoreCorrelativa = create<CorrelativaStore>(
  devtools(
    (set) => ({
      correlativasMateria: [],
      correlativaAActualizar: null,
      correlativaSeleccionada: null,
      materiasCorrelativas: [],
      loading: false,
      error: null,
      getCorrelativas: async (idMateria: string) => {
        try {
          set({ loading: true })
          const correlativasMateria = await obtenerCorrelativas(idMateria)
          set({ correlativasMateria })
          set({ loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      getMateriasCorrelativas: async (idMateria: string) => {
        try {
          set({ loading: true })
          const materiasCorrelativas = await obtenerMateriasCorrelativas(idMateria)
          set({ materiasCorrelativas })
          set({ loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setCorrelativas: async (correlativasMateria: Correlativa[]) => set({
        correlativasMateria
      }),
      setToUpdate: (correlativaAActualizar: Correlativa | null) => set({ correlativaAActualizar }),
      setCorrelativaSeleccionada: (correlativaSeleccionada: Correlativa | null) => set({ correlativaSeleccionada })
    })
  )
)
