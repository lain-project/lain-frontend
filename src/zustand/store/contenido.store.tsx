import create from 'zustand'
import { devtools } from 'zustand/middleware'
import { obtenerContenido } from '@/api/contenido.service'
import { Contenido } from '@/model/contenido.model'

export const useStoreContenido = create<any>(
  devtools(
    (set) => ({
      contenidosMateria: [],
      contenidoAActualizar: null,
      contenidoSeleccionado: null,
      loading: false,
      error: null,
      getContenidos: async (idUnidad: string) => {
        try {
          set({ loading: true })
          const contenidosMateria = await obtenerContenido(idUnidad)
          set({ contenidosMateria })
          set({ loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setContenidos: async (contenidosMateria: Contenido[]) => set({
        contenidosMateria
      }),
      setToUpdate: (contenidoAActualizar: Contenido) => set({ contenidoAActualizar }),
      setContenidoSeleccionado: (contenidoSeleccionado: Contenido) => set({ contenidoSeleccionado })
    })
  )
)
