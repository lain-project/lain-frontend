import create from 'zustand'
import { devtools } from 'zustand/middleware'
import { TipoEntidad } from '@/model/entidad.model'
import { obtenerEntidades } from '@/api/chatbot.service'

interface ChatbotStore {
  entidades: TipoEntidad[]
  entidadSeleccionada: TipoEntidad | null
  loading: boolean
  error: any
  obtenerEntidades: () => Promise<void>,
  setEntidadSeleccionada: (entidadSeleccionada: TipoEntidad | null) => void,
  updateLoading: (state:boolean) => void
}

export const useStoreChatbot = create<ChatbotStore>(
  devtools(
    (set) => ({
      entidades: [],
      entidadSeleccionada: null,
      loading: false,
      error: null,
      obtenerEntidades: async () => {
        try {
          set({ loading: true })
          const entidades = await obtenerEntidades()
          set({ entidades })
          set({ loading: false })
        } catch (error) {
          set({ error, loading: false })
        }
      },
      setEntidadSeleccionada: (entidadSeleccionada: TipoEntidad | null) => set({ entidadSeleccionada }),
      updateLoading: (state) => set({loading: state})
    })
  )
)
